﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using IdentityLayer.Entities;
using IdentityLayer.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace IdentityLayer.Services
{
    public class UserService : IUserService<FullInfoUser>
    {
        private readonly IRepository<User> userRepository;
        private readonly UserManager<ArchiveIdentityUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public UserService(IRepository<User> userRepository, UserManager<ArchiveIdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.userRepository = userRepository;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        public User AddUser(FullInfoUser fullInfoUser)
        {
            var result = userManager.CreateAsync(fullInfoUser.ArchiveIdentityUser, fullInfoUser.Password).Result;
            if (result == IdentityResult.Success)
            {

                fullInfoUser.User.ArchiveIdentityUserId = userManager.FindByNameAsync(fullInfoUser.ArchiveIdentityUser.UserName).Result.Id;
                userManager.AddToRoleAsync(userManager.FindByIdAsync(fullInfoUser.User.ArchiveIdentityUserId).Result, fullInfoUser.Role);
                return userRepository.Add(fullInfoUser.User);
            }
            Dictionary<string, string> errors = new Dictionary<string, string>();

            foreach (var item in result.Errors)
            {
                errors.Add(item.Code, item.Description);
            }
            throw new AddingUserException(errors);
        }

        public async Task<User> AddUserAsync(FullInfoUser fullInfoUser)
        {
            var result = await userManager.CreateAsync(fullInfoUser.ArchiveIdentityUser, fullInfoUser.Password);
            if ( result == IdentityResult.Success)
            {
                fullInfoUser.User.ArchiveIdentityUserId = (await userManager.FindByNameAsync(fullInfoUser.ArchiveIdentityUser.UserName)).Id;
                await userManager.AddToRoleAsync((await userManager.FindByIdAsync(fullInfoUser.User.ArchiveIdentityUserId)), fullInfoUser.Role);
                return await userRepository.AddAsync(fullInfoUser.User);
            }
            Dictionary<string, string> errors = new Dictionary<string, string>();

            foreach (var item in result.Errors)
            {
                errors.Add(item.Code, item.Description);
            }
            throw new AddingUserException(errors);
        }
    }
}
