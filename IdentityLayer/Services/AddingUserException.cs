﻿using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityLayer.Services
{
    public class AddingUserException:Exception
    {
        public Dictionary<string, string> AddingUserErrors { get; }

        public AddingUserException(Dictionary<string, string> addingUserErrors):base("Couldn't add user")
        {
            AddingUserErrors = addingUserErrors;
        }
    }
}
