﻿using DocumentsArchiverCore.Entities;
using IdentityLayer.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityLayer.Models
{
    public class FullInfoUser
    {
        public User User { get; set; }
        public ArchiveIdentityUser ArchiveIdentityUser { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }
}
