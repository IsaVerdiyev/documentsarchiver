﻿using IdentityLayer.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace IdentityLayer.Context
{
    public class UsersDbContext: IdentityDbContext<ArchiveIdentityUser>
    {
        public UsersDbContext(DbContextOptions<UsersDbContext> options): base(options)
        {

        }
    }
}
