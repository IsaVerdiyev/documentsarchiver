﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentsArchiverInfrastructure.Migrations
{
    public partial class CorrectingDocumentScanFile : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentScanFiles_Documents_DocumentId",
                table: "DocumentScanFiles");

            migrationBuilder.AlterColumn<int>(
                name: "DocumentId",
                table: "DocumentScanFiles",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentScanFiles_Documents_DocumentId",
                table: "DocumentScanFiles",
                column: "DocumentId",
                principalTable: "Documents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentScanFiles_Documents_DocumentId",
                table: "DocumentScanFiles");

            migrationBuilder.AlterColumn<int>(
                name: "DocumentId",
                table: "DocumentScanFiles",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentScanFiles_Documents_DocumentId",
                table: "DocumentScanFiles",
                column: "DocumentId",
                principalTable: "Documents",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
