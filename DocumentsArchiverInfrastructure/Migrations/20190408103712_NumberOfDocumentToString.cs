﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DocumentsArchiverInfrastructure.Migrations
{
    public partial class NumberOfDocumentToString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "NumberOfDocument",
                table: "Documents",
                nullable: true,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "NumberOfDocument",
                table: "Documents",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);
        }
    }
}
