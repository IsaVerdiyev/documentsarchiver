﻿using DocumentsArchiverCore.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverInfrastructure.Data
{
    public class ArchiveDb: DbContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<Folder> Folders { get; set; }
        public DbSet<FolderType> FolderTypes { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<Floor> Floors { get; set; }
        public DbSet<KindOfDocument> KindsOfDocument { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<Organization> Organizations { get; set; }
        public DbSet<ReportingAct> ReportingActs { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Shelf> Shelves { get; set; }
        public DbSet<Shelving> Shelvings { get; set; }
        public DbSet<ShelvingColumn> ShelvingColumns { get; set; }
        public DbSet<StructuralDepartment> StructuralDepartments { get; set; }
        public DbSet<TypeOfDocument> TypesOfDocument { get; set; }
        public DbSet<DocumentScanFile> DocumentScanFiles { get; set; }
        public DbSet<FolderScanFile> FolderScanFiles { get; set; }
        public DbSet<ReportingScanFile> ReportingScanFiles { get; set; }
        public DbSet<User> Users { get; set; }


        public ArchiveDb(DbContextOptions<ArchiveDb> options): base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Document>().HasOne(d => d.ReportingAct).WithOne(r => r.Document).HasForeignKey<ReportingAct>(ra => ra.Id);
            modelBuilder.Entity<ReportingAct>().HasOne(a => a.ReportingScanFile).WithOne(f => f.ReportingAct).HasForeignKey<ReportingScanFile>(rf => rf.Id);

            base.OnModelCreating(modelBuilder);
        }
    }
}
