﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DocumentsArchiverInfrastructure.Data
{
    public class EfRepository<T> : IRepository<T> where T : BaseEntity
    {

        ArchiveDb archiveDb;

        public EfRepository(ArchiveDb archiveDb)
        {
            this.archiveDb = archiveDb;
        }

        public T Add(T entity)
        {
            T added = archiveDb.Set<T>().Add(entity).Entity;
            archiveDb.SaveChanges();
            return added;
        }

        public async Task<T> AddAsync(T entity)
        {
            T added = archiveDb.Set<T>().Add(entity).Entity;
            await archiveDb.SaveChangesAsync();
            return added;
        }

        public int Count(ISpecification<T> spec)
        {
            return ApplySpecification(spec).Count();
        }

        public Task<int> CountAsync(ISpecification<T> spec)
        {
            return ApplySpecification(spec).CountAsync();
        }

        public void Delete(T entity)
        {

            archiveDb.Set<T>().Remove(entity);
            archiveDb.SaveChanges();
        }

        public async Task DeleteAsync(T entity)
        {
            archiveDb.Set<T>().Remove(entity);
            await archiveDb.SaveChangesAsync();
        }

        public T GetById(int id)
        {
            T found = archiveDb.Set<T>().Find(id);
            return found;
        }

        public async Task<T> GetByIdAsync(int id)
        {
            T found = await archiveDb.Set<T>().FindAsync(id);
            return found;
        }

        public T GetSingleBySpec(ISpecification<T> spec)
        {
            return ApplySpecification(spec).FirstOrDefault();
        }

        public async Task<T> GetSingleBySpecAsync(ISpecification<T> spec)
        {
            return await ApplySpecification(spec).FirstOrDefaultAsync();
        }

        public IReadOnlyList<T> List(ISpecification<T> spec)
        {
            return ApplySpecification(spec).ToList();
        }

        public IReadOnlyList<T> ListAll()
        {
            return archiveDb.Set<T>().ToList();
        }

        public async Task<IReadOnlyList<T>> ListAllAsync()
        {
            return await archiveDb.Set<T>().AsNoTracking().ToListAsync();
        }

        public async Task<IReadOnlyList<T>> ListAsync(ISpecification<T> spec)
        {
            return await ApplySpecification(spec).AsNoTracking().ToListAsync();
        }

        public T Update(T entity)
        {
            archiveDb.Entry<T>(entity).State = EntityState.Modified;
            archiveDb.SaveChanges();
            return entity;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            archiveDb.Entry<T>(entity).State = EntityState.Modified;
            await archiveDb.SaveChangesAsync();
            return entity;
        }

        private IQueryable<T> ApplySpecification(ISpecification<T> specification)
        {
            return SpecificationEvaluator<T>.GetQuery(archiveDb.Set<T>().AsQueryable(), specification);
        }

    }
}
