﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Interfaces
{
    public interface ISpecificFileSaver
    {
        IEnumerable<string> SaveFiles(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption);
        Task<IEnumerable<string>> SaveFilesAsync(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption);

        string SaveFile(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption);
        Task<string> SaveFileAsync(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption);
    }
}
