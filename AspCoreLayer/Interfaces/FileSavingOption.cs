﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Interfaces
{
    public enum FileSavingOption
    {
        Replace,
        CreateNew
    }
}
