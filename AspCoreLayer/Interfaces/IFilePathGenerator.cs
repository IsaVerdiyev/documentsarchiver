﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Interfaces
{
    public interface IFilePathGenerator
    {
        string GenerateNewFilePath(int idOfItem, string filename, string folderPath);
        string GenerateFilePath(int idOfItem, string filename, string folderPath);
    }
}
