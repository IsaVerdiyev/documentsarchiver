﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Interfaces
{
    public interface IFileSaver
    {
        IEnumerable<string> SaveFiles(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption, string folderPath);
        Task<IEnumerable<string>> SaveFilesAsync(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption, string folderPath);

        string SaveFile(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption, string folderPath);
        Task<string> SaveFileAsync(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption, string folderPath);
    }
}
