﻿using AspCoreLayer.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Services
{
    public class FileSaver : IFileSaver
    {
        private readonly IFilePathGenerator fileNameGenerator;
     
        public FileSaver(IFilePathGenerator fileNameGenerator)
        {
            this.fileNameGenerator = fileNameGenerator;
        }

        public string SaveFile(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption, string folderPath)
        {
            string filePath;
            if (fileSavingOption == FileSavingOption.CreateNew)
            { 
                filePath = fileNameGenerator.GenerateNewFilePath(idOfItem, Path.GetFileName(formFile.FileName), folderPath);
            }
            else if(fileSavingOption == FileSavingOption.Replace)
            {
                filePath = fileNameGenerator.GenerateFilePath(idOfItem, Path.GetFileName(formFile.FileName), folderPath);
            }
            else
            {
                throw new Exception("FileSavingOption not passed to method SaveFile");
            }
            return SaveFileByPathFromFormFile(filePath, formFile);
        }

        public async Task<string> SaveFileAsync(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption, string folderPath)
        {
            string filePath;
            if (fileSavingOption == FileSavingOption.CreateNew)
            {
                filePath = fileNameGenerator.GenerateNewFilePath(idOfItem, Path.GetFileName(formFile.FileName), folderPath);
            }
            else if (fileSavingOption == FileSavingOption.Replace)
            {
                filePath = fileNameGenerator.GenerateFilePath(idOfItem, Path.GetFileName(formFile.FileName), folderPath);
            }
            else
            {
                throw new Exception("FileSavingOption not passed to method SaveFile");
            }
            return await SaveFileByPathFromFormFileAsync(filePath, formFile);
        }

        public IEnumerable<string> SaveFiles(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption, string folderPath)
        {
            List<string> filePathes = new List<string>();
            foreach(var formFile in formFiles)
            {
                filePathes.Add(SaveFile(idOfItem, formFile, fileSavingOption, folderPath));
            }
            return filePathes;
        }

        public async Task<IEnumerable<string>> SaveFilesAsync(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption, string folderPath)
        {
            List<string> filePathes = new List<string>();
            foreach (var formFile in formFiles)
            {
                filePathes.Add(await SaveFileAsync(idOfItem, formFile, fileSavingOption, folderPath));
            }
            return filePathes;
        }


        private string SaveFileByPathFromFormFile(string filePath, IFormFile formFile)
        {
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                formFile.CopyTo(stream);
            }
            return filePath;
        }

        private async Task<string> SaveFileByPathFromFormFileAsync(string filePath, IFormFile formFile)
        {
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                await formFile.CopyToAsync(stream);
            }
            return filePath;
        }
    }
}
