﻿using AspCoreLayer.Interfaces;
using Microsoft.AspNetCore.Hosting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Services
{
    public class FilePathGenerator : IFilePathGenerator
    {
        private string firstPartOfPath;
        private readonly IHostingEnvironment hostingEnvironment;

        public FilePathGenerator(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
            firstPartOfPath = hostingEnvironment.ContentRootPath;
        }
        public string GenerateNewFilePath(int idOfItem, string filename, string folderPath)
        {
            string directoryPath = $"{firstPartOfPath}{folderPath}/{idOfItem}";
            Directory.CreateDirectory(directoryPath);
            string filePath = $"{directoryPath}/{filename}";
            if (!File.Exists(filePath))
            {
                return filePath;
            }
            else
            {
                string initialFileName = filename;
                string extension = "";
                if (initialFileName.Contains('.'))
                {
                    extension = initialFileName.Substring(initialFileName.LastIndexOf('.'));
                    initialFileName = initialFileName.Remove(initialFileName.LastIndexOf('.'));
                }
                filePath = GetUniqueFileName($"{directoryPath}/{initialFileName}");
                filePath = filePath + extension;
                return filePath;
            }

        }

        public string GenerateFilePath(int idOfItem, string filename, string folderPath)
        {
            string directoryPath = $"{firstPartOfPath}{folderPath}/{idOfItem}";
            Directory.CreateDirectory(directoryPath);
            return $"{directoryPath}/{filename}";
        }

        private string GetUniqueFileName(string path)
        {
            Random random = new Random();
            int number = random.Next(0, int.MaxValue);
            string newFilePath = $"{path}{number}";
            if (File.Exists(newFilePath))
            {
                return GetUniqueFileName(path);
            }
            return newFilePath;
        }

    }
}
