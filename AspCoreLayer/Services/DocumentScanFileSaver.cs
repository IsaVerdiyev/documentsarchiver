﻿using AspCoreLayer.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Services
{
    public class DocumentScanFileSaver: IDocumentScanFileSaver
    {
        private readonly string folderPath = "/DocumentScanFiles";
        private readonly IFileSaver fileSaver;

        public DocumentScanFileSaver(IFileSaver fileSaver)
        {
            this.fileSaver = fileSaver;
        }

        public string SaveFile(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption)
        {
            return fileSaver.SaveFile(idOfItem, formFile, fileSavingOption, folderPath);
        }

        public async Task<string> SaveFileAsync(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption)
        {
            return await fileSaver.SaveFileAsync(idOfItem, formFile, fileSavingOption, folderPath);
        }

        public IEnumerable<string> SaveFiles(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption)
        {
            return fileSaver.SaveFiles(idOfItem, formFiles, fileSavingOption, folderPath);
        }

        public async Task<IEnumerable<string>> SaveFilesAsync(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption)
        {
            return await fileSaver.SaveFilesAsync(idOfItem, formFiles, fileSavingOption, folderPath);
        }
    }
}
