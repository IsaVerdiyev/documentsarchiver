﻿using AspCoreLayer.Interfaces;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Services
{
    public class ReportScanFileSaver : IReportScanFileSaver
    {
        private readonly string folderPath = "/ReportScanFiles";
        private readonly IFileSaver fileSaver;

        public ReportScanFileSaver(IFileSaver fileSaver)
        {
            this.fileSaver = fileSaver;
        }

        public string SaveFile(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption)
        {
            return fileSaver.SaveFile(idOfItem, formFile, fileSavingOption, folderPath);
        }

        public async Task<string> SaveFileAsync(int idOfItem, IFormFile formFile, FileSavingOption fileSavingOption)
        {
            return await fileSaver.SaveFileAsync(idOfItem, formFile, fileSavingOption, folderPath);
        }

        public IEnumerable<string> SaveFiles(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption)
        {
            return fileSaver.SaveFiles(idOfItem, formFiles, fileSavingOption, folderPath);
        }

        public async Task<IEnumerable<string>> SaveFilesAsync(int idOfItem, IEnumerable<IFormFile> formFiles, FileSavingOption fileSavingOption)
        {
            return await fileSaver.SaveFilesAsync(idOfItem, formFiles, fileSavingOption, folderPath);
        }
    }
}
