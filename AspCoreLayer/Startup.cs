﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspCoreLayer.Interfaces;
using AspCoreLayer.Services;
using DocumentsArchiverCore.Interfaces;
using DocumentsArchiverInfrastructure.Data;
using IdentityLayer.Context;
using IdentityLayer.Entities;
using IdentityLayer.Models;
using IdentityLayer.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Services;

namespace AspCoreLayer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            
            string dataConnectionString = Configuration.GetConnectionString("DataConnection");

            services.AddDbContext<ArchiveDb>(options =>
            options.UseSqlServer(dataConnectionString), ServiceLifetime.Transient
            );

            string identityConnectionString = Configuration.GetConnectionString("IdentityConnection");

            services.AddDbContext<UsersDbContext>(options => 
                options.UseSqlServer(identityConnectionString), ServiceLifetime.Transient
            );


            services.AddTransient(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddTransient<IFolderService, FolderService>();
            services.AddTransient<IFolderTypeService, FolderTypeService>();
            services.AddTransient<IOrganizationService, OrganizationService>();
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<ICityService, CityService>();
            services.AddTransient<IRoomService, RoomService>();
            services.AddTransient<IFileSaver, FileSaver>();
            services.AddTransient<IReportScanFileSaver, ReportScanFileSaver>();
            services.AddTransient<IFolderScanFileSaver, FolderScanFileSaver>();
            services.AddTransient<IDocumentScanFileSaver, DocumentScanFileSaver>();
            services.AddTransient<IFilePathGenerator, FilePathGenerator>();
            services.AddTransient<IFolderScanFileService, FolderScanFileService>();
            services.AddTransient<IUserService<FullInfoUser>, UserService>();
            services.AddTransient<IFileDeleter, FileDeleter>();
            services.AddTransient<IDocumentsService, DocumentsService>();
            services.AddTransient<IKindsOfDocumentService, KindsOfDocumentService>();
            services.AddTransient<ITypesOfDocumentService, TypesOfDocumentService>();
            services.AddTransient<IStructuralDepartmentsService, StructuralDepartmentsService>();
            services.AddTransient<IDocumentScanFilesService, DocumentScanFilesService>();

            services.AddIdentity<ArchiveIdentityUser, IdentityRole>().
                AddEntityFrameworkStores<UsersDbContext>().AddDefaultTokenProviders();
            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = "/Identity/Account/Login";
            });

            var mvc = services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            mvc.AddJsonOptions(options => {
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                
                routes.MapRoute(
                    name: "areas",
                    template: "{area=FoldersArea}/{controller=Folder}/{action=Folders}/{id?}"
                );

                
                //routes.MapRoute(
                //    name: "default",
                //    template: "{controller=Folder}/{action=Folders}/{id?}"
                //);
            });
        }
    }
}
