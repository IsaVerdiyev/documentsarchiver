﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.DocumentsArea.ViewModels
{
    public class AddDocumentScanfileViewModel
    {
        public int DocumentId { get; set; }
        public IFormFile File { get; set; }
    }
}
