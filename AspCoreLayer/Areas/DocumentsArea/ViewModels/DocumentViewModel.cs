﻿using DocumentsArchiverCore.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.DocumentsArea.ViewModels
{
    public class DocumentViewModel
    {
        public int Id { get; set; }

        [Required]
        public int FolderId { get; set; }
        
        [Required]
        public string NumberOfDocument { get; set; }

        [Required]
        public int KindOfDocumentId { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        public DateTime DateOfDocument { get; set; }

        [Required]
        public int TypeOfDocumentId { get; set; }

        [Required]
        public int StructuralDepartmentId { get; set; }

        [Required]
        public string IntroducerInitials { get; set; }

        [Required]
        public string ExecuterInitials { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        public DateTime ExecutionDate { get; set; }

        public string Note { get; set; }

        public IFormFile ReportingActScanFile { get; set; }
        public IEnumerable<IFormFile> ScanFiles { get; set; }


        public IEnumerable<KindOfDocument> KindsOfDocument { get; set; }
        public IEnumerable<TypeOfDocument> TypesOfDocument { get; set; }
        public IEnumerable<StructuralDepartment> StructuralDepartments { get; set; }

    }
}
