﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.DocumentsArea.ViewModels
{
    public class FolderDocumentsViewModel
    {
        public int FolderId { get; set; }
        public IEnumerable<Document> Documents { get; set; }
    }
}
