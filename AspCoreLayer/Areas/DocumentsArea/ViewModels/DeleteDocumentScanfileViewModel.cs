﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.DocumentsArea.ViewModels
{
    public class DeleteDocumentScanfileViewModel
    {
        public int DocumentId { get; set; }
        public int DeletedDocumentScanfileId { get; set; }
    }
}
