﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.DocumentsArea.ViewModels
{
    public class AddStructuralDepartmentViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public int NomenclatureNumber { get; set; }

        public string ReturnUrl { get; set; }
    }
}
