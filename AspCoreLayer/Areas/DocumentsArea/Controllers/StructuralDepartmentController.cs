﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspCoreLayer.Areas.DocumentsArea.ViewModels;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace AspCoreLayer.Areas.DocumentsArea.Controllers
{
    [Area("DocumentsArea")]
    public class StructuralDepartmentController : Controller
    {
        private readonly IStructuralDepartmentsService structuralDepartmentsService;

        public StructuralDepartmentController(IStructuralDepartmentsService structuralDepartmentsService)
        {
            this.structuralDepartmentsService = structuralDepartmentsService;
        }

        [HttpGet]
        public IActionResult Add(string returnUrl = null)
        {
            return View(new AddStructuralDepartmentViewModel { ReturnUrl = returnUrl});
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddStructuralDepartmentViewModel addStructuralDepartmentViewModel)
        {
            if (ModelState.IsValid)
            {
                var addedStructuralDepartment = new StructuralDepartment { Name = addStructuralDepartmentViewModel.Name, NomenclatureNumber = addStructuralDepartmentViewModel.NomenclatureNumber };

                await structuralDepartmentsService.AddStructuralDepartmentAsync(addedStructuralDepartment);
                if (!string.IsNullOrEmpty(addStructuralDepartmentViewModel.ReturnUrl) && Url.IsLocalUrl(addStructuralDepartmentViewModel.ReturnUrl))
                {
                    return Redirect(addStructuralDepartmentViewModel.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Add", "StructuralDepartment");
                }
            }
            else
            {
                return View(addStructuralDepartmentViewModel);
            }
        }
    }
}