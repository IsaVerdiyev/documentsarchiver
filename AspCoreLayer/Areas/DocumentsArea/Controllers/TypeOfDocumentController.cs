﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspCoreLayer.Areas.DocumentsArea.ViewModels;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AspCoreLayer.Areas.DocumentsArea.Controllers
{
    [Area("DocumentsArea")]
    [Authorize]
    public class TypeOfDocumentController : Controller
    {
        private readonly ITypesOfDocumentService typesOfDocumentService;

        public TypeOfDocumentController(ITypesOfDocumentService typesOfDocumentService)
        {
            this.typesOfDocumentService = typesOfDocumentService;
        }

        [HttpGet]
        public IActionResult Add(string returnUrl = null)
        {
            return View(new AddTypeOfDocumentViewModel { ReturnUrl = returnUrl});
        }


        [HttpPost]
        public async Task<IActionResult> Add(AddTypeOfDocumentViewModel addTypeOfDocumentViewModel)
        {
            var addedTypeOfDocument = new TypeOfDocument { Name = addTypeOfDocumentViewModel.Name };

            await typesOfDocumentService.AddTypeOfDocumentAsync(addedTypeOfDocument);
            if (!string.IsNullOrEmpty(addTypeOfDocumentViewModel.ReturnUrl) && Url.IsLocalUrl(addTypeOfDocumentViewModel.ReturnUrl))
            {
                return Redirect(addTypeOfDocumentViewModel.ReturnUrl);
            }
            else
            {
                return RedirectToAction("Add", "TypeOfDocument");
            }
        }
       
    }
}