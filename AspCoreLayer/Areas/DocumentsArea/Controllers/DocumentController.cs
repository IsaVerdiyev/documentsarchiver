﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspCoreLayer.Areas.DocumentsArea.ViewModels;
using AspCoreLayer.Interfaces;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AspCoreLayer.Areas.DocumentsArea.Controllers
{
    [Area("DocumentsArea")]
    [Authorize]
    [Route("[area]/[controller]/[action]")]
    public class DocumentController : Controller
    {
        private readonly IDocumentsService documentsService;
        private readonly IKindsOfDocumentService kindsOfDocumentService;
        private readonly ITypesOfDocumentService typesOfDocumentService;
        private readonly IStructuralDepartmentsService structuralDepartmentsService;
        private readonly IReportScanFileSaver reportScanFileSaver;
        private readonly IDocumentScanFileSaver documentScanFileSaver;
        private readonly IFolderService folderService;

        public DocumentController(IDocumentsService documentsService, IKindsOfDocumentService kindsOfDocumentService, ITypesOfDocumentService typesOfDocumentService, IStructuralDepartmentsService structuralDepartmentsService, IReportScanFileSaver reportScanFileSaver, IDocumentScanFileSaver documentScanFileSaver, IFolderService folderService)
        {
            this.documentsService = documentsService;
            this.kindsOfDocumentService = kindsOfDocumentService;
            this.typesOfDocumentService = typesOfDocumentService;
            this.structuralDepartmentsService = structuralDepartmentsService;
            this.reportScanFileSaver = reportScanFileSaver;
            this.documentScanFileSaver = documentScanFileSaver;
            this.folderService = folderService;
        }


        [Route("{folderId}")]
        [HttpGet]
        public async Task<IActionResult> FolderDocuments(int folderId)
        {
            ViewData["folderId"] = folderId;
            if(!await CheckIfFolderExists(folderId))
            {
                return View("FolderNotFound");
            }
            IEnumerable<Document> documents = await documentsService.GetDocumentsOfFolderAsync(folderId);
            ViewData["folderDocuments"] = documents;
            return View();
        }

        
        public async Task<IActionResult> Add(int folderId)
        {
            if (!await CheckIfFolderExists(folderId))
            {
                return View("../../../FoldersArea/Views/Folder/FolderNotFound", folderId);
            }

            DocumentViewModel documentViewModel = new DocumentViewModel { FolderId = folderId, DateOfDocument = new DateTime(2015, 01, 01), ExecutionDate = new DateTime(2015, 01, 01) };
            return View(await FillInDataInDocumentViewModelAsync(documentViewModel));
        }

        [HttpPost]
        public async Task<IActionResult> Add(DocumentViewModel documentViewModel)
        {
            if (ModelState.IsValid)
            {
                if (!await CheckIfFolderExists(documentViewModel.FolderId))
                {
                    return View("../../../FoldersArea/Views/Folder/FolderNotFound", documentViewModel.FolderId);
                }

                var typeOfDocumentGettingTask = typesOfDocumentService.GetTypeOfDocumentIdAsync(documentViewModel.TypeOfDocumentId);
                var kindOfDocumentGettingTask = kindsOfDocumentService.GetKindOfDocumentByIdAsync(documentViewModel.KindOfDocumentId);
                var structuralDepartmentGettingTask = structuralDepartmentsService.GetStructuralDepartmentByIdAsync(documentViewModel.StructuralDepartmentId);

                TypeOfDocument typeOfDocument = await typeOfDocumentGettingTask;
                KindOfDocument kindOfDocument = await kindOfDocumentGettingTask;
                StructuralDepartment structuralDepartment = await structuralDepartmentGettingTask;

                if (typeOfDocument == null)
                {
                    return NotFound("Type of document not found");
                }
                else if (kindOfDocument == null)
                {
                    return NotFound("Kind of document not found");
                }
                else if (structuralDepartment == null)
                {
                    return NotFound("Structural department not found");
                }


                Document document = new Document
                {
                    DateOfDocument = documentViewModel.DateOfDocument,
                    ExecuterInitials = documentViewModel.ExecuterInitials,
                    ExecutionDate = documentViewModel.ExecutionDate,
                    FolderId = documentViewModel.FolderId,
                    IntroducerInitials = documentViewModel.IntroducerInitials,
                    KindOfDocumentId = documentViewModel.KindOfDocumentId,
                    Note = documentViewModel.Note,
                    NumberOfDocument = documentViewModel.NumberOfDocument,
                    StructuralDepartmentId = documentViewModel.StructuralDepartmentId,
                    TypeOfDocumentId = documentViewModel.TypeOfDocumentId
                };

                document = await documentsService.AddDocumentAsync(document);

                if (documentViewModel.ReportingActScanFile != null)
                {
                    string filePath = await reportScanFileSaver.SaveFileAsync(document.Id, documentViewModel.ReportingActScanFile, FileSavingOption.CreateNew);
                    document.ReportingAct = new ReportingAct { ReportingScanFile = new ReportingScanFile { ScanFilePath = filePath } };
                    document = await documentsService.ModifyDocumentAsync(document);
                }

                if (documentViewModel.ScanFiles != null)
                {
                    IEnumerable<string> filePathes = await documentScanFileSaver.SaveFilesAsync(document.Id, documentViewModel.ScanFiles, FileSavingOption.CreateNew);
                    document.ScanFiles = filePathes.Select(p => new DocumentScanFile { ScanFilePath = p }).ToList();
                    document = await documentsService.ModifyDocumentAsync(document);
                }

                return Redirect(Url.Action("Folders", "Folder", new { folderId = document.FolderId, area = "FoldersArea" }));
            }
            else
            {
                return View(await FillInDataInDocumentViewModelAsync(documentViewModel));
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int documentId)
        {
            Document document = await documentsService.GetDocumentByIdAsync(documentId);
            if (document == null)
            {
                return View("DocumentNotFound", documentId);
            }

            return View(await GetDocumentViewModelFromDocument(document));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(DocumentViewModel documentViewModel)
        {

            if (ModelState.IsValid)
            {

                Document document = await documentsService.GetDocumentByIdAsync(documentViewModel.Id);
                if (document == null)
                {
                    return View("DocumentNotFound", documentViewModel.Id);
                }

                var typeOfDocumentGettingTask = typesOfDocumentService.GetTypeOfDocumentIdAsync(documentViewModel.TypeOfDocumentId);
                var kindOfDocumentGettingTask = kindsOfDocumentService.GetKindOfDocumentByIdAsync(documentViewModel.KindOfDocumentId);
                var structuralDepartmentGettingTask = structuralDepartmentsService.GetStructuralDepartmentByIdAsync(documentViewModel.StructuralDepartmentId);

                TypeOfDocument typeOfDocument = await typeOfDocumentGettingTask;
                KindOfDocument kindOfDocument = await kindOfDocumentGettingTask;
                StructuralDepartment structuralDepartment = await structuralDepartmentGettingTask;

                if (typeOfDocument == null)
                {
                    return NotFound("Type of document not found");
                }
                else if (kindOfDocument == null)
                {
                    return NotFound("Kind of document not found");
                }
                else if (structuralDepartment == null)
                {
                    return NotFound("Structural department not found");
                }




                document.DateOfDocument = documentViewModel.DateOfDocument;
                document.ExecuterInitials = documentViewModel.ExecuterInitials;
                document.ExecutionDate = documentViewModel.ExecutionDate;
                document.FolderId = documentViewModel.FolderId;
                document.IntroducerInitials = documentViewModel.IntroducerInitials;
                document.KindOfDocumentId = documentViewModel.KindOfDocumentId;
                document.Note = documentViewModel.Note;
                document.NumberOfDocument = documentViewModel.NumberOfDocument;
                document.StructuralDepartmentId = documentViewModel.StructuralDepartmentId;
                document.TypeOfDocumentId = documentViewModel.TypeOfDocumentId;

                await documentsService.ModifyDocumentAsync(document);
                


                return Redirect(Url.Action("Folders", "Folder", new { area = "FoldersArea", folderId = document.FolderId }));
            }
            else
            {
                return View(await FillInDataInDocumentViewModelAsync(documentViewModel));
            }
        }

        private async Task<bool> CheckIfFolderExists(int folderId)
        {
            return await folderService.CheckIfFolderExistsByIdAsync(folderId);
        }

        private async Task<DocumentViewModel> GetDocumentViewModelFromDocument(Document document)
        {
            DocumentViewModel documentViewModel = new DocumentViewModel
            {
                DateOfDocument = document.DateOfDocument,
                ExecuterInitials = document.ExecuterInitials,
                ExecutionDate = document.ExecutionDate,
                FolderId = document.FolderId,
                Id = document.Id,
                IntroducerInitials = document.IntroducerInitials,
                KindOfDocumentId = document.KindOfDocumentId,
                Note =document.Note,
                NumberOfDocument = document.NumberOfDocument,
                StructuralDepartmentId = document.StructuralDepartmentId,
                TypeOfDocumentId = document.TypeOfDocumentId
                
            };
            return await FillInDataInDocumentViewModelAsync(documentViewModel);

        }

        private async Task<DocumentViewModel> FillInDataInDocumentViewModelAsync(DocumentViewModel documentViewModel)
        {
            var kindsOfDocumentGettingTask = kindsOfDocumentService.GetAllKindsOfDocumentAsync();
            var typesOfDocumentGettingTask = typesOfDocumentService.GetAllTypesOfDocumentAsync();
            var structuralDepartmentsGettingTask = structuralDepartmentsService.GetAllStructuralDepartmentsAsync();

            documentViewModel.KindsOfDocument = await kindsOfDocumentGettingTask;
            documentViewModel.TypesOfDocument = await typesOfDocumentGettingTask;
            documentViewModel.StructuralDepartments = await structuralDepartmentsGettingTask;
            return documentViewModel;
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]DeleteDocumentViewModel deleteDocumentViewModel)
        {
            await documentsService.DeleteDocumentAsync(deleteDocumentViewModel.DocumentId);
            FolderDocumentsViewModel folderDocumentsViewModel = new FolderDocumentsViewModel
            {
                Documents = await documentsService.GetDocumentsOfFolderAsync(deleteDocumentViewModel.FolderId),
                FolderId = deleteDocumentViewModel.FolderId
        };
            return PartialView("_DocumentsTable", folderDocumentsViewModel);
        }


        [HttpPost]
        public async Task<IActionResult> GetDocumentsOfFolder([FromBody]FolderDocumentsViewModel folderDocumentsViewModel)
        {
            if (ModelState.IsValid)
            {
               folderDocumentsViewModel.Documents = await documentsService.GetDocumentsOfFolderAsync(folderDocumentsViewModel.FolderId);
                return PartialView("_DocumentsTable", folderDocumentsViewModel);
            }
            else
            {
                return new EmptyResult();
            }
        }

    }
}