﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspCoreLayer.Areas.DocumentsArea.ViewModels;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AspCoreLayer.Areas.KindOfDocumentsArea.Controllers
{

    [Area("DocumentsArea")]
    [Authorize]
    public class KindOfDocumentController : Controller
    {
        private readonly IKindsOfDocumentService kindsOfDocumentService;

        public KindOfDocumentController(IKindsOfDocumentService kindsOfDocumentService)
        {
            this.kindsOfDocumentService = kindsOfDocumentService;
        }

        [HttpGet]
        public IActionResult Add(string returnUrl = null)
        {
            return View(new AddKindOfDocumentViewModel { ReturnUrl = returnUrl});
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddKindOfDocumentViewModel addKindOfDocumentViewModel)
        {
            if (ModelState.IsValid)
            {
                var addedKindOfDocument = new KindOfDocument { Name = addKindOfDocumentViewModel.Name };

                await kindsOfDocumentService.AddKindOfDocumentAsync(addedKindOfDocument);
                if (!string.IsNullOrEmpty(addKindOfDocumentViewModel.ReturnUrl) && Url.IsLocalUrl(addKindOfDocumentViewModel.ReturnUrl))
                {
                    return Redirect(addKindOfDocumentViewModel.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("");
                }
            }
            else
            {
                return View(addKindOfDocumentViewModel);
            }
        }
    }
}