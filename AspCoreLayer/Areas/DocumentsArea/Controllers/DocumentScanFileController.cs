﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AspCoreLayer.Areas.DocumentsArea.ViewModels;
using AspCoreLayer.Interfaces;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static System.Net.Mime.MediaTypeNames;

namespace AspCoreLayer.Areas.DocumentsArea.Controllers
{
    [Area("DocumentsArea")]
    [Authorize]
    [Route("[area]/[controller]/[action]")]
    public class DocumentScanFileController : Controller
    {
        private readonly IDocumentScanFilesService documentScanFilesService;
        private readonly IDocumentsService documentsService;
        private readonly IDocumentScanFileSaver documentScanFileSaver;

        public DocumentScanFileController(IDocumentScanFilesService documentScanFilesService, IDocumentsService documentsService, IDocumentScanFileSaver documentScanFileSaver)
        {
            this.documentScanFilesService = documentScanFilesService;
            this.documentsService = documentsService;
            this.documentScanFileSaver = documentScanFileSaver;
        }

        [HttpGet]
        public async Task<IActionResult> ScanFiles(int documentId)
        {
            ViewData["DocumentId"] = documentId;
            ViewData["Message"] = $"Sənəd id: {documentId}";
            return View(await documentScanFilesService.GetDocumentScanFilesOfDocumentAsync(documentId));
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody]DeleteDocumentScanfileViewModel deleteDocumentScanfileViewModel)
        {
            int documentId= deleteDocumentScanfileViewModel.DocumentId;
            int deletedScanFileId = deleteDocumentScanfileViewModel.DeletedDocumentScanfileId;
            await documentScanFilesService.DeleteDocumentScanfileByIdAsync(deletedScanFileId);
            return PartialView("_ScanFilesTable", await documentScanFilesService.GetDocumentScanFilesOfDocumentAsync(documentId));
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddDocumentScanfileViewModel addDocumentScanfileViewModel)
        {
            Document foundDocument = await documentsService.GetDocumentByIdWithScanFilesAsync(addDocumentScanfileViewModel.DocumentId);
            if (foundDocument == null)
            {
                return NotFound();
            }

            string filePath = await documentScanFileSaver.SaveFileAsync(foundDocument.Id, addDocumentScanfileViewModel.File, FileSavingOption.CreateNew);
            foundDocument.ScanFiles.Add(new DocumentScanFile { ScanFilePath = filePath });

            await documentsService.ModifyDocumentAsync(foundDocument);

            return PartialView("_ScanFilesTable", await documentScanFilesService.GetDocumentScanFilesOfDocumentAsync(foundDocument.Id));
        }

        [HttpGet]
        public IActionResult Download(string filePath)
        {
            if (ModelState.IsValid)
            {
                //var folderScanFile = await folderScanFilesService.GetFolderScanFileAsync(/*int.Parse(*/downloadFolderScanfileViewModel.ScanFileId/*)*/);

                byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

                return File(fileBytes, Text.Plain, Path.GetFileName(filePath));

                //return new JsonResult(folderScanFile.ScanFilePath);
            }
            else
            {
                return StatusCode(422);
            }
        }
    }
}