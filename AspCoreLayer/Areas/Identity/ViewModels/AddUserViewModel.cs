﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.Identity.ViewModels
{
    public class AddUserViewModel
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Surname { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Role { get; set; }

        [Required]
        [UIHint("Password")]
        public string Password { get; set; }

        [Required]
        [UIHint("Password")]
        [Compare("Password")]
        public string ConfirmationPassword { get; set; }
    }
}
