﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspCoreLayer.Areas.Identity.ViewModels;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using IdentityLayer.Entities;
using IdentityLayer.Models;
using IdentityLayer.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AspCoreLayer.Areas.Identity.Controllers
{
    
    [Area("Identity")]
    public class AccountController : Controller
    {
        private readonly IUserService<FullInfoUser> userService;
        private readonly SignInManager<ArchiveIdentityUser> signInManager;
        private readonly UserManager<ArchiveIdentityUser> userManager;
        private readonly RoleManager<IdentityRole> roleManager;

        public AccountController(IUserService<FullInfoUser> userService, SignInManager<ArchiveIdentityUser> signInManager, UserManager<ArchiveIdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            this.userService = userService;
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.roleManager = roleManager;
        }

        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var result = await signInManager.PasswordSignInAsync(loginViewModel.UserName, loginViewModel.Password, false, false);
                if (result.Succeeded)
                {
                    // проверяем, принадлежит ли URL приложению
                    if (!string.IsNullOrEmpty(loginViewModel.ReturnUrl) && Url.IsLocalUrl(loginViewModel.ReturnUrl))
                    {
                        return Redirect(loginViewModel.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Folders", "Folder", new { area = "FoldersArea" });
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Səhv login və ya parol");
                }
            }
            return View(loginViewModel);
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult AddUser()
        {
            ViewData["Roles"] = roleManager.Roles;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> AddUser(AddUserViewModel addUserViewModel)
        {
            ViewData["Roles"] = roleManager.Roles;
            if (ModelState.IsValid)
            {
                FullInfoUser fullInfoUser = new FullInfoUser
                {
                    ArchiveIdentityUser = new ArchiveIdentityUser
                    {
                        UserName = addUserViewModel.UserName
                    },
                    User = new User
                    {
                        Name = addUserViewModel.Name,
                        Surname = addUserViewModel.Surname
                    },
                    Password = addUserViewModel.Password,
                    Role = addUserViewModel.Role
                };
                try
                {
                    User addedUser = await userService.AddUserAsync(fullInfoUser);

                    return View("UserSuccessfullyAdded");
                }
                catch (AddingUserException ex)
                {
                    foreach(var item in ex.AddingUserErrors)
                    {
                        ModelState.AddModelError(string.Empty, item.Value);
                    }
                }
            }
            return View(addUserViewModel);
        }

        public async Task<IActionResult> LogOut()
        {
            await signInManager.SignOutAsync();
            return Redirect(Url.Action("Folders", "Folder", new { area = "FoldersArea"}));
        }

       
    }
}