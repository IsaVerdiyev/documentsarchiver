﻿using AspCoreLayer.Attributes;
using DocumentsArchiverCore.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class FolderViewModel
    {
        public int Id { get; set; }

        [Required]
        public int FolderTypeId { get; set; }

        [Required]
        public int OrganizationId { get; set; }

        [Required]
        public int LocationId { get; set; }

        [Required]
        public int Floor { get; set; }

        [Required]
        public int RoomId { get; set; }

        [Required]
        public int ShelvingColumnNumber { get; set; }

        [Required]
        public int ShelvingNumber { get; set; }

        [Required]
        public int ShelfNumber { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        public DateTime DateFrom { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd'/'MM'/'yyyy}")]
        [DateGreaterThan("DateFrom")]
        public DateTime DateTo { get; set; }

        public string Note { get; set; }
        
        public IEnumerable<FolderType> FolderTypes { get; set; }
        public IEnumerable<Organization> Organizations { get; set; }
        public IEnumerable<Location> Locations { get; set; }
        public IEnumerable<Room> Rooms { get; set; }

        public IEnumerable<IFormFile> ScanFiles { get; set; }
    }
}
