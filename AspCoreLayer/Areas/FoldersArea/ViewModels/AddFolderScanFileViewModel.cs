﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class AddFolderScanFileViewModel
    {
        public int FolderId { get; set; }
        public IFormFile File { get; set; }
    }
}
