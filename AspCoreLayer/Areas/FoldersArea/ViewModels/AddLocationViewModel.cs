﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class AddLocationViewModel
    {
        [Required]
        public int CityId { get; set; }

        [Required]
        public string NewAddress { get; set; }

        public string ReturnUrl { get; set; }

        public IEnumerable<City> Cities { get; set; } 
    }
}
