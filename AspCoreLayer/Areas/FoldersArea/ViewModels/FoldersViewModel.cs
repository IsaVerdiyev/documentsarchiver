﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class FoldersViewModel
    {
        public IEnumerable<Folder> Folders { get; set; }
        public int SelectedFolderId { get; set; }
    }
}
