﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class AddRoomViewModel
    {

        [Required]
        public string RoomName { get; set; }

        public string ReturnUrl { get; set; }
    }
}
