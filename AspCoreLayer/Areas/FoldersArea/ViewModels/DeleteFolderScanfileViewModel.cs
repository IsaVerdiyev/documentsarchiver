﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class DeleteFolderScanfileViewModel
    {
        public int FolderId { get; set; }
        public int DeletedScanFileId { get; set; }
    }
}
