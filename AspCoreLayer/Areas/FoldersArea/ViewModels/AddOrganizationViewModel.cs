﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class AddOrganizationViewModel
    {
        [Required]
        public string NewOrganizationName { get; set; }

        public string ReturnUrl { get; set; }
    }
}
