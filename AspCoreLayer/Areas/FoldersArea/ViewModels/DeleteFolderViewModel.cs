﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class DeleteFolderViewModel
    {
        public int FolderId { get; set; }
    }
}
