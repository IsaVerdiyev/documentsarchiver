﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class AddCityViewModel
    {
        [Required]
        public string NewCityName { get; set; }

        public string ReturnUrl { get; set; }
    }
}
