﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.ViewModels
{
    public class DownloadFolderScanfileViewModel
    {
        public int ScanFileId { get; set; }
        public int FolderId { get; set; }
    }
}
