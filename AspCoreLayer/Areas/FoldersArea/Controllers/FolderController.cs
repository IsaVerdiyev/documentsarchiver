﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AspCoreLayer.Areas.FoldersArea.ViewModels;
using AspCoreLayer.Interfaces;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AspCoreLayer.Areas.FoldersArea.Controllers
{

    [Area("FoldersArea")]
    [Authorize]
    public class FolderController : Controller
    {
        private readonly IFolderService folderService;
        private readonly IFolderTypeService folderTypeService;
        private readonly IOrganizationService organizationService;
        private readonly ILocationService locationService;
        private readonly IRoomService roomService;
        private readonly IFolderScanFileSaver folderScanFileSaver;

        public FolderController(IFolderService folderService, IFolderTypeService folderTypeService, IOrganizationService organizationService, ILocationService locationService, IRoomService roomService, IFolderScanFileSaver folderScanFileSaver)
        {
            this.folderService = folderService;
            this.folderTypeService = folderTypeService;
            this.organizationService = organizationService;
            this.locationService = locationService;
            this.roomService = roomService;
            this.folderScanFileSaver = folderScanFileSaver;
        }

        [HttpGet]
        public async Task<IActionResult> Folders(int? folderId = null)
        {
            IReadOnlyList<Folder> folders = await folderService.GetFoldersAsync();
            int selectedFolderId;
            if (folderId == null)
            {
                var firstFolder = folders.FirstOrDefault();
                selectedFolderId = firstFolder != null ? firstFolder.Id : -1;
            }
            else
            {
                selectedFolderId = folderId.Value;
            }
            return View(new FoldersViewModel { Folders = folders, SelectedFolderId = selectedFolderId });
        }

        [HttpGet]
        public async Task<IActionResult> Add()
        {
            return View(await UpdateFolderViewModel(new FolderViewModel { DateFrom = new DateTime(2015, 01, 01), DateTo =  new DateTime(2016, 01, 01)}));
        }

        [HttpPost]
        public async Task<IActionResult> Add(FolderViewModel folderViewModel)
        {
            if (ModelState.IsValid)
            {
                var folderTypeGettingTask = folderTypeService.GetFolderTypeByIdAsync(folderViewModel.FolderTypeId);
                var organizationGettingTask = organizationService.GetOrganizationByIdAsync(folderViewModel.OrganizationId);
                var locationGettingTask = locationService.GetLocationByIdAsync(folderViewModel.LocationId);
                var roomGettingTask = roomService.GetRoomByIdAsync(folderViewModel.RoomId);
                FolderType folderType = await folderTypeGettingTask;
                Organization organization = await organizationGettingTask;
                Location location = await locationGettingTask;
                Room room = await roomGettingTask;
                if (folderType == null)
                {
                    return new NotFoundObjectResult("Container type not found");
                }
                if (organization == null)
                {
                    return new NotFoundObjectResult("Organization not found");
                }
                if (location == null)
                {
                    return new NotFoundObjectResult("Location not found");
                }

                if (room == null)
                {
                    return new NotFoundObjectResult("Room not found");
                }


                Folder folder = new Folder
                {
                    FolderTypeId = folderType.Id,
                    DateFrom = folderViewModel.DateFrom,
                    DateTo = folderViewModel.DateTo,
                    Floor = new Floor { Number = folderViewModel.Floor },
                    LocationId = location.Id,
                    Documents = new List<Document>(),
                    Note = folderViewModel.Note,
                    OrganizationId = organization.Id,
                    RoomId = room.Id,
                    Shelf = new Shelf { Number = folderViewModel.ShelfNumber },
                    Shelving = new Shelving { Number = folderViewModel.ShelvingNumber },
                    ShelvingColumn = new ShelvingColumn { Number = folderViewModel.ShelvingColumnNumber }
                };
                folder = await folderService.AddFolderAsync(folder);

                if (folderViewModel.ScanFiles != null)
                {
                    IEnumerable<string> filePathes = await folderScanFileSaver.SaveFilesAsync(folder.Id, folderViewModel.ScanFiles, FileSavingOption.CreateNew);
                    folder.FolderScanFiles = filePathes.Select(p => new FolderScanFile { ScanFilePath = p }).ToList();
                    await folderService.ModifyFolderAsync(folder);
                }
                return Redirect(Url.Action("Folders"));
            }
            else
            {
                await UpdateFolderViewModel(folderViewModel);
                return View(folderViewModel);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int folderId)
        {
            Folder folder = await folderService.GetFolderByIdWithDataAsync(folderId);
            if (folder == null)
            {
                return View("FolderNotFound", folderId);
            }
            return View(await UpdateFolderViewModel(folder, new FolderViewModel()));
        }

        [HttpPost]
        public async Task<IActionResult> Edit(FolderViewModel folderViewModel)
        {
            if (ModelState.IsValid)
            {
                Folder folder = await folderService.GetFolderByIdWithDataAsync(folderViewModel.Id);
                if (folder == null)
                {
                    return View("FolderNotFound", folderViewModel.Id);
                }
                var folderTypeGettingTask = folderTypeService.GetFolderTypeByIdAsync(folderViewModel.FolderTypeId);
                var organizationGettingTask = organizationService.GetOrganizationByIdAsync(folderViewModel.OrganizationId);
                var locationGettingTask = locationService.GetLocationByIdAsync(folderViewModel.LocationId);
                var roomGettingTask = roomService.GetRoomByIdAsync(folderViewModel.RoomId);
                FolderType folderType = await folderTypeGettingTask;
                Organization organization = await organizationGettingTask;
                Location location = await locationGettingTask;
                Room room = await roomGettingTask;
                if (folderType == null)
                {
                    return new NotFoundObjectResult("Container type not found");
                }
                if (organization == null)
                {
                    return new NotFoundObjectResult("Organization not found");
                }
                if (location == null)
                {
                    return new NotFoundObjectResult("Location not found");
                }

                if (room == null)
                {
                    return new NotFoundObjectResult("Room not found");
                }


                folder.FolderTypeId = folderType.Id;
                folder.DateFrom = folderViewModel.DateFrom;
                folder.DateTo = folderViewModel.DateTo;
                folder.Floor = new Floor { Number = folderViewModel.Floor };
                folder.LocationId = location.Id;
                folder.Documents = new List<Document>();
                folder.Note = folderViewModel.Note;
                folder.OrganizationId = organization.Id;
                folder.RoomId = room.Id;
                folder.Shelf = new Shelf { Number = folderViewModel.ShelfNumber };
                folder.Shelving = new Shelving { Number = folderViewModel.ShelvingNumber };
                folder.ShelvingColumn = new ShelvingColumn { Number = folderViewModel.ShelvingColumnNumber };

                await folderService.ModifyFolderAsync(folder);

                
                return Redirect(Url.Action("Folders", new { folderId = folderViewModel.Id}));
            }
            else
            {
                await UpdateFolderViewModel(folderViewModel);
                return View(folderViewModel);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            IReadOnlyList<Folder> folders = await folderService.GetFoldersAsync();
            return PartialView("_FoldersTable", folders);
        }

        private async Task<FolderViewModel> UpdateFolderViewModel(FolderViewModel folderViewModel)
        {
            var folderTypesGettingTask = folderTypeService.GetFolderTypesAsync();
            var organizationsTask = organizationService.GetOrganizationsAsync();
            var locationsGettingTask = locationService.GetLocationsAsync();
            var roomsGettingTask = roomService.GetRoomsAsync();
            folderViewModel.FolderTypes = await folderTypesGettingTask;
            folderViewModel.Organizations = await organizationsTask;
            folderViewModel.Locations = await locationsGettingTask;
            folderViewModel.Rooms = await roomsGettingTask;
            return folderViewModel;
        }

        private async Task<FolderViewModel> UpdateFolderViewModel(Folder folder, FolderViewModel folderViewModel)
        {
            folderViewModel.Id = folder.Id;
            folderViewModel.DateFrom = folder.DateFrom;
            folderViewModel.DateTo = folder.DateTo;
            folderViewModel.Floor = folder.Floor.Number;
            folderViewModel.FolderTypeId = folder.FolderTypeId;
            folderViewModel.LocationId = folder.LocationId;
            folderViewModel.Note = folder.Note;
            folderViewModel.OrganizationId = folder.OrganizationId;
            folderViewModel.RoomId = folder.RoomId;
            folderViewModel.ShelfNumber = folder.Shelf.Number;
            folderViewModel.ShelvingColumnNumber = folder.ShelvingColumn.Number;
            folderViewModel.ShelvingNumber = folder.Shelving.Number;
            return await UpdateFolderViewModel(folderViewModel);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteFolderViewModel deleteFolderViewModel)
        {
            await folderService.DeleteFolderAsync(deleteFolderViewModel.FolderId);
            return PartialView("_FoldersTable", await folderService.GetFoldersAsync());
        }

    }

}