﻿using System.Threading.Tasks;
using AspCoreLayer.Areas.FoldersArea.ViewModels;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AspCoreLayer.Areas.RoomsArea.Controllers
{
    [Area("FoldersArea")]
    [Authorize]
    public class RoomController : Controller
    {
        private readonly IRoomService roomService;

        public RoomController(IRoomService roomService)
        {
            this.roomService = roomService;
        }

        [HttpGet]
        public IActionResult Add(string returnUrl)
        {
            return View(new AddRoomViewModel { ReturnUrl = returnUrl});
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddRoomViewModel addRoomViewModel)
        {
            if (ModelState.IsValid)
            {
                Room addedRoom = new Room { RoomNumber = addRoomViewModel.RoomName };
                await roomService.AddRoomAsync(addedRoom);
                if (!string.IsNullOrEmpty(addRoomViewModel.ReturnUrl) && Url.IsLocalUrl(addRoomViewModel.ReturnUrl))
                {
                    return Redirect(addRoomViewModel.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Folders", "Folder", new { area = "FoldersArea" });
                }
            }
            else
            {
                return View(addRoomViewModel);
            }
        }
    }
}
