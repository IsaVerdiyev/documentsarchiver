﻿using System.Threading.Tasks;
using AspCoreLayer.Areas.FoldersArea.ViewModels;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AspCoreLayer.Areas.FolderTypesArea.Controllers
{
    [Area("FoldersArea")]
    [Authorize]
    public class FolderTypeController : Controller
    {
        private readonly IFolderTypeService folderTypeService;

        public FolderTypeController(IFolderTypeService folderTypeService)
        {
            this.folderTypeService = folderTypeService;
        }

        [HttpGet]
        public IActionResult Add(string returnUrl)
        {
            return View(new AddFolderTypeViewModel { ReturnUrl = returnUrl});
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddFolderTypeViewModel addFolderTypeViewModel)
        {
            if (ModelState.IsValid)
            {
                FolderType folderType = new FolderType { Name = addFolderTypeViewModel.FolderTypeName };
                await folderTypeService.AddFolderTypeAsync(folderType);
                if (!string.IsNullOrEmpty(addFolderTypeViewModel.ReturnUrl) && Url.IsLocalUrl(addFolderTypeViewModel.ReturnUrl))
                {
                    return Redirect(addFolderTypeViewModel.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Folders", "Folder", new { area = "FoldersArea" });
                }
            }
            else
            {
                return View(addFolderTypeViewModel);
            }
        }
    }
}