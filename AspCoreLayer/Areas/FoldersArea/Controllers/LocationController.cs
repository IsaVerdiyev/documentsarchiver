﻿using System.Threading.Tasks;
using AspCoreLayer.Areas.FoldersArea.ViewModels;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AspCoreLayer.Areas.LocationsArea.Controllers
{
    [Area("FoldersArea")]
    [Authorize]
    public class LocationController : Controller
    {
        private readonly ILocationService locationService;
        private readonly ICityService cityService;

        public LocationController(ILocationService locationService, ICityService cityService)
        {
            this.locationService = locationService;
            this.cityService = cityService;
        }

        [HttpGet]
        public async Task<IActionResult> Add(string returnUrl)
        {
            AddLocationViewModel addLocationViewModel = new AddLocationViewModel { ReturnUrl = returnUrl };
            addLocationViewModel.Cities = await cityService.GetCitiesAsync();
            return View(addLocationViewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddLocationViewModel addLocationViewModel)
        {
            if (ModelState.IsValid)
            {
                City city = await cityService.GetCityByIdAsync(addLocationViewModel.CityId);
                if (city == null)
                {
                    return new NotFoundObjectResult("Şəhər tapılmadı bu cür id ilə");
                }
                Location newLocation = new Location { CityId = city.Id, Address = addLocationViewModel.NewAddress };
                await locationService.AddLocationAsync(newLocation);
                if (!string.IsNullOrEmpty(addLocationViewModel.ReturnUrl) && Url.IsLocalUrl(addLocationViewModel.ReturnUrl))
                {
                    return Redirect(addLocationViewModel.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Folders", "Folder", new { area = "FoldersArea" });
                }
            }
            else
            {
                addLocationViewModel.Cities = await cityService.GetCitiesAsync();
                return View(addLocationViewModel);
            }
        }
    }
}