﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using AspCoreLayer.Areas.FoldersArea.ViewModels;
using AspCoreLayer.Interfaces;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using static System.Net.Mime.MediaTypeNames;

namespace AspCoreLayer.Areas.FoldersArea.Controllers
{
    [Area("FoldersArea")]
    [Route("[controller]")]
    [Authorize]
    public class FolderScanFileController : Controller
    {
        private readonly IFolderScanFileService folderScanFilesService;
        private readonly IFolderScanFileSaver folderScanFileSaver;
        private readonly IFolderService folderService;
        private readonly IFileDeleter fileDeleter;

        public FolderScanFileController(IFolderScanFileService folderScanFilesService, IFolderScanFileSaver folderScanFileSaver, IFolderService folderService, IFileDeleter fileDeleter)
        {
            this.folderScanFilesService = folderScanFilesService;
            this.folderScanFileSaver = folderScanFileSaver;
            this.folderService = folderService;
            this.fileDeleter = fileDeleter;
        }

        [HttpGet] 
        [Route("[action]/{folderId?}")]
        public async Task<IActionResult> ScanFiles(int folderId)
        {
            IEnumerable<FolderScanFile> scanFilesOfFolder = await folderScanFilesService.GetFolderScanFilesAsync(folderId);
            ViewData["Message"] = $"Qovluğ id: {folderId}";
            ViewData["FolderId"] = folderId;
            return View(scanFilesOfFolder);
        }

        [HttpPost]
        public async Task<IActionResult> AddScanFile(AddFolderScanFileViewModel addFolderScanFileViewModel)
        {
            Folder foundFolder = await folderService.GetFolderByIdWithScanFilesAsync(addFolderScanFileViewModel.FolderId);
            if(foundFolder == null)
            {
                return NotFound();
            }

            string filePath = await folderScanFileSaver.SaveFileAsync(foundFolder.Id, addFolderScanFileViewModel.File, FileSavingOption.CreateNew);
            foundFolder.FolderScanFiles.Add(new FolderScanFile { ScanFilePath = filePath });
           
            await folderService.ModifyFolderAsync(foundFolder);
            
            return PartialView("_ScanFilesTable",await folderScanFilesService.GetFolderScanFilesAsync(foundFolder.Id));
        }

        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] DeleteFolderScanfileViewModel deleteFolderScanfileViewModel)
        {
            int folderId = deleteFolderScanfileViewModel.FolderId;
            int deletedScanFileId = deleteFolderScanfileViewModel.DeletedScanFileId;
            await folderScanFilesService.DeleteFolderScanfileByIdAsync(deletedScanFileId);
            return PartialView("_ScanFilesTable", await folderScanFilesService.GetFolderScanFilesAsync(folderId));
        }

        [HttpGet]
        public IActionResult Download( string filePath)
        {
            if (ModelState.IsValid)
            {
                //var folderScanFile = await folderScanFilesService.GetFolderScanFileAsync(/*int.Parse(*/downloadFolderScanfileViewModel.ScanFileId/*)*/);

                byte[] fileBytes = System.IO.File.ReadAllBytes(filePath);

                return File(fileBytes, Text.Plain, Path.GetFileName(filePath));

                //return new JsonResult(folderScanFile.ScanFilePath);
            }
            else
            {
                return StatusCode(422);
            }
        }

    }
}