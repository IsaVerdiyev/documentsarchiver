﻿using System.Threading.Tasks;
using AspCoreLayer.Areas.FoldersArea.ViewModels;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace AspCoreLayer.Areas.FoldersArea.Controllers
{
    [Area("FoldersArea")]
    [Authorize]
    public class OrganizationController : Controller
    {
        private readonly IOrganizationService organizationService;

        public OrganizationController(IOrganizationService organizationService)
        {
            this.organizationService = organizationService;
        }

        [HttpGet]
        public IActionResult Add(string returnUrl)
        {
            return View(new AddOrganizationViewModel { ReturnUrl = returnUrl});
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddOrganizationViewModel addOrganizationViewModel)
        {
            if (ModelState.IsValid)
            {
                Organization newOrganization = new Organization { Name = addOrganizationViewModel.NewOrganizationName };
                await organizationService.AddOrganizationAsync(newOrganization);
                if (!string.IsNullOrEmpty(addOrganizationViewModel.ReturnUrl) && Url.IsLocalUrl(addOrganizationViewModel.ReturnUrl))
                {
                    return Redirect(addOrganizationViewModel.ReturnUrl);
                }
                else
                {
                    return RedirectToAction("Folders", "Folder", new { area = "FoldersArea" });
                }
            }
            else
            {
                return View(addOrganizationViewModel);
            }
        }
    }
}