﻿using AspCoreLayer.Areas.FoldersArea.ViewModels;
using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AspCoreLayer.Areas.FoldersArea.Controllers
{
    [Area("FoldersArea")]
    [Authorize]
    public class CityController: Controller
    {
        private readonly ICityService cityService;

        public CityController(ICityService cityService)
        {
            this.cityService = cityService;
        }



        [HttpGet]
        public IActionResult Add(string returnUrl)
        {
            return View(new AddCityViewModel { ReturnUrl = returnUrl});
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddCityViewModel addCityViewModel)
        {
            City newCity = new City { Name = addCityViewModel.NewCityName };
            await cityService.AddCityAsync(newCity);
            return Redirect(Url.Action("Add", "Location", new { returnUrl = addCityViewModel.ReturnUrl}));
        }
    }
}
