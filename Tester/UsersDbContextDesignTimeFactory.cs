﻿using IdentityLayer.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tester
{
    public class UsersDbContextDesignTimeFactory: DesignTimeDbContextFactory<UsersDbContext>
    {
        public UsersDbContextDesignTimeFactory(): base("IdentityConnection")
        {

        }
    }
}
