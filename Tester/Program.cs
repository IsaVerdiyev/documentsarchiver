﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using DocumentsArchiverInfrastructure.Data;
using IdentityLayer.Context;
using IdentityLayer.Entities;
using IdentityLayer.Models;
using IdentityLayer.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Services;
using System;
using System.IO;
using System.Linq;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
            // установка пути к текущему каталогу
            builder.SetBasePath(Directory.GetCurrentDirectory());
            // получаем конфигурацию из файла appsettings.json
            builder.AddJsonFile("appsettings.json");
            // создаем конфигурацию
            var config = builder.Build();
            // получаем строку подключения
            string dataConnection = config.GetConnectionString("DataConnection");

            

            string identityConnection = config.GetConnectionString("IdentityConnection");
           
            var services = new ServiceCollection();

            services.AddDbContext<ArchiveDb>(options =>
                options.UseSqlServer(dataConnection),ServiceLifetime.Transient
            );
            services.AddDbContext<UsersDbContext>(options =>
                options.UseSqlServer(identityConnection),ServiceLifetime.Transient
            );
            services.AddIdentity<ArchiveIdentityUser, IdentityRole>().
                AddEntityFrameworkStores<UsersDbContext>();

            services.AddTransient(typeof(IRepository<>), typeof(EfRepository<>));
            services.AddTransient<IFolderService, FolderService>();
            services.AddTransient<IFolderTypeService, FolderTypeService>();
            services.AddTransient<IOrganizationService, OrganizationService>();
            services.AddTransient<ILocationService, LocationService>();
            services.AddTransient<ICityService, CityService>();
            services.AddTransient<IRoomService, RoomService>();
            services.AddTransient<IFolderScanFileService, FolderScanFileService>();

            services.AddTransient<IUserService<FullInfoUser>, UserService>();

            //UsersDbContext usersDbContext = new UsersDbContext(identityOptions);

            var provider = services.BuildServiceProvider();

            

            var userService = provider.GetService<IUserService<FullInfoUser>>();
            FullInfoUser fullInfoUser = new FullInfoUser
            {
                User = new User { Name = "Isa", Surname = "Verdiyev" },
                ArchiveIdentityUser = new ArchiveIdentityUser { UserName = "IsaAdmin" },
                Password = "SDsysw2dsk!!"
            };
            User user = userService.AddUser(fullInfoUser);
            if (user != null)
            {
                Console.WriteLine(user.ArchiveIdentityUserId);
            }
            else
            {
                Console.WriteLine("Couldn't add");
            }
        }
    }
}
