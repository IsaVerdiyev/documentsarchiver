﻿using DocumentsArchiverInfrastructure.Data;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tester
{
    public class ArchiveDbDesignTimeFactory: DesignTimeDbContextFactory<ArchiveDb>
    {
        public ArchiveDbDesignTimeFactory(): base("DataConnection")
        {

        }
    }
}
