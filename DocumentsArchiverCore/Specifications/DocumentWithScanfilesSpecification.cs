﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocumentsArchiverCore.Specifications
{
    public class DocumentWithScanfilesSpecification : BaseSpecification<Document>
    {
        public DocumentWithScanfilesSpecification(int documentId) : base(d => d.Id == documentId)
        {
            Includes.Add(d => d.ScanFiles);
        }

        public DocumentWithScanfilesSpecification(Document document) : base(d => d.Id == document.Id)
        {
            Includes.Add(d => d.ScanFiles);
        }
    }
}
