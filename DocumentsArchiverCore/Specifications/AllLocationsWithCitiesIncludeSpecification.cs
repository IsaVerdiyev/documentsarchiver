﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocumentsArchiverCore.Specifications
{
    public class AllLocationsWithCitiesIncludeSpecification : BaseSpecification<Location>
    {
        public AllLocationsWithCitiesIncludeSpecification() : base(l => true)
        {
            Includes.Add(l => l.City);
        }
    }
}
