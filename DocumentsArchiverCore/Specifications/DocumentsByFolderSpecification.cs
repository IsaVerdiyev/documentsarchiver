﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocumentsArchiverCore.Specifications
{
    public class DocumentsByFolderSpecification : BaseSpecification<Document>
    {
        public DocumentsByFolderSpecification(int folderId) : base(d => d.FolderId == folderId)
        {
            SetIncludes();
        }

        public DocumentsByFolderSpecification(Folder folder): base(d => d.FolderId == folder.Id)
        {
            SetIncludes();
        }


        private void SetIncludes()
        {
            Includes.Add(d => d.KindOfDocument);
            Includes.Add(d => d.Folder);
            Includes.Add(d => d.ReportingAct);
            Includes.Add(d => d.StructuralDepartment);
            Includes.Add(d => d.TypeOfDocument);
        }
    }
}
