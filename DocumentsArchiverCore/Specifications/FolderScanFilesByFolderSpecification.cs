﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocumentsArchiverCore.Specifications
{
    public class FolderScanFilesByFolderSpecification : BaseSpecification<FolderScanFile>
    {
        public FolderScanFilesByFolderSpecification(int folderId) : base(fs => fs.FolderId == folderId)
        {
        }

        public FolderScanFilesByFolderSpecification(Folder folder): base(fs => fs.FolderId == folder.Id)
        {

        }
    }
}
