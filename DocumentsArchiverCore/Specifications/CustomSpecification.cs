﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocumentsArchiverCore.Specifications
{
    public class CustomSpecification<T> : BaseSpecification<T> where T : BaseEntity
    {
        public CustomSpecification(Expression<Func<T, bool>> criteria, IEnumerable<Expression<Func<T, object>>> Includes = null, IEnumerable<string> IncludeStrings = null) : base(criteria)
        {
            if (Includes != null)
            {
                this.Includes.AddRange(Includes);
            }
            if (IncludeStrings != null)
            {
                this.IncludeStrings.AddRange(IncludeStrings);
            }
        }
    }
}
