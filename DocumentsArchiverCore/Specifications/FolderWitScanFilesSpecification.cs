﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocumentsArchiverCore.Specifications
{
    public class FolderWitScanFilesSpecification : BaseSpecification<Folder>
    {
        public FolderWitScanFilesSpecification(int folderId) : base(f => f.Id == folderId)
        {
            Includes.Add(f => f.FolderScanFiles);
        }

        public FolderWitScanFilesSpecification(Folder folder): base (f => f.Id == folder.Id)
        {
            Includes.Add(f => f.FolderScanFiles);
        }
    }
}
