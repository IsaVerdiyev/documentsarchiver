﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocumentsArchiverCore.Specifications
{
    public class FoldersWithAllIncludeSpecification : BaseSpecification<Folder>
    {
        public FoldersWithAllIncludeSpecification() : base(f => true)
        {
            Includes.Add(f => f.FolderType);
            Includes.Add(f => f.Documents);
            Includes.Add(f => f.Floor);
            Includes.Add(f => f.Location.City);
            Includes.Add(f => f.Organization);
            Includes.Add(f => f.Room);
            Includes.Add(f => f.FolderScanFiles);
            Includes.Add(f => f.Shelf);
            Includes.Add(f => f.Shelving);
            Includes.Add(f => f.ShelvingColumn);
        }
    }
}
