﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace DocumentsArchiverCore.Specifications
{
    public class DocumentScanFilesByDocumentSpecification : BaseSpecification<DocumentScanFile>
    {
        public DocumentScanFilesByDocumentSpecification(int documentId) : base(f => f.DocumentId == documentId)
        {
        }

        public DocumentScanFilesByDocumentSpecification(Document document) : base(f => f.DocumentId == document.Id)
        {
        }
    }
}
