﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Specifications
{
    public class FolderWithAllWithoutScanFilesSpecification: BaseSpecification<Folder>
    {
        public FolderWithAllWithoutScanFilesSpecification(int folderId): base(f => f.Id == folderId)
        {
            SetIncludes();
        }

        public FolderWithAllWithoutScanFilesSpecification(Folder folder): base(f => f.Id == folder.Id)
        {
            SetIncludes();
        }

        private void SetIncludes()
        {
            Includes.Add(f => f.FolderType);
            Includes.Add(f => f.Documents);
            Includes.Add(f => f.Floor);
            Includes.Add(f => f.Location.City);
            Includes.Add(f => f.Organization);
            Includes.Add(f => f.Room);
            Includes.Add(f => f.Shelf);
            Includes.Add(f => f.Shelving);
            Includes.Add(f => f.ShelvingColumn);
        }
    }
}
