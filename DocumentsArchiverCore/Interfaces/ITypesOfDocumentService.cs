﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface ITypesOfDocumentService
    {
        IEnumerable<TypeOfDocument> GetAllTypesOfDocument();
        Task<IEnumerable<TypeOfDocument>> GetAllTypesOfDocumentAsync();

        TypeOfDocument AddTypeOfDocument(TypeOfDocument typeOfDocument);
        Task<TypeOfDocument> AddTypeOfDocumentAsync(TypeOfDocument typeOfDocument);

        TypeOfDocument GetTypeOfDocumentById(int typeOfDocumentId);
        Task<TypeOfDocument> GetTypeOfDocumentIdAsync(int typeOfDocumentId);
    }
}
