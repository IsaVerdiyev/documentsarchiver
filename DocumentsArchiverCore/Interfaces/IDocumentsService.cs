﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IDocumentsService
    {
        IEnumerable<Document> GetDocumentsOfFolder(int folderId);
        Task<IEnumerable<Document>> GetDocumentsOfFolderAsync(int folderId);

        Document AddDocument(Document document);
        Task<Document> AddDocumentAsync(Document document);

        Document ModifyDocument(Document document);
        Task<Document> ModifyDocumentAsync(Document document);

        void DeleteDocument(int documentId);
        Task DeleteDocumentAsync(int documentId);

        Document GetDocumentById(int documentId);
        Task<Document> GetDocumentByIdAsync(int documentId);

        Document GetDocumentByIdWithScanFiles(int documentId);
        Task<Document> GetDocumentByIdWithScanFilesAsync(int documentId);
    }
}
