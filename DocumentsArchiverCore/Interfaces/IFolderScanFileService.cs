﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IFolderScanFileService
    {
        IEnumerable<FolderScanFile> GetScanFilesOfFolder(int folderId);
        Task<IEnumerable<FolderScanFile>> GetFolderScanFilesAsync(int folderId);

        void DeleteFolderScanfileById(int folderScanfileId);
        Task DeleteFolderScanfileByIdAsync(int folderScanfileId);

        FolderScanFile GetFolderScanFileById(int folderScanFileId);
        Task<FolderScanFile> GetFolderScanFileAsync(int folderScanFileId);
    }
}
