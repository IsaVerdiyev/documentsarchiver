﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IStructuralDepartmentsService
    {
        IEnumerable<StructuralDepartment> GetAllStructuralDepartments();
        Task<IEnumerable<StructuralDepartment>> GetAllStructuralDepartmentsAsync();

        StructuralDepartment AddStructuralDepartment(StructuralDepartment structuralDepartment);
        Task<StructuralDepartment> AddStructuralDepartmentAsync(StructuralDepartment structuralDepartment);

        StructuralDepartment GetStructuralDepartmentById(int structuralDepartmentId);
        Task<StructuralDepartment> GetStructuralDepartmentByIdAsync(int structuralDepartmentId);
    }
}
