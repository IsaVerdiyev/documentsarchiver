﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IRoomService
    {
        IReadOnlyList<Room> GetRooms();
        Task<IReadOnlyList<Room>> GetRoomsAsync();

        Room GetRoomById(int id);
        Task<Room> GetRoomByIdAsync(int id);

        Room AddRoom(Room room);
        Task<Room> AddRoomAsync(Room room);
    }
}
