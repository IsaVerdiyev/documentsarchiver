﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IFolderTypeService
    {
        IReadOnlyList<FolderType> GetFolderTypes();
        Task<IReadOnlyList<FolderType>> GetFolderTypesAsync();

        FolderType GetFolderTypeById(int id);
        Task<FolderType> GetFolderTypeByIdAsync(int id);

        void AddFolderType(FolderType containerType);
        Task AddFolderTypeAsync(FolderType containerType);
    }
}
