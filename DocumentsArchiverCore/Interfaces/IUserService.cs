﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IUserService<T>
    {
        User AddUser(T user);
        Task<User> AddUserAsync(T user);
    }
}
