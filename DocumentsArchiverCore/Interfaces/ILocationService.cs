﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface ILocationService
    {
        IReadOnlyList<Location> GetLocations();
        Task<IReadOnlyList<Location>> GetLocationsAsync();

        Location GetLocationById(int id);
        Task<Location> GetLocationByIdAsync(int id);

        Location AddLocation(Location location);
        Task<Location> AddLocationAsync(Location location);
    }
}
