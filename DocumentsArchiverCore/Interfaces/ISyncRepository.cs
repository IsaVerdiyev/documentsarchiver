﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Interfaces
{
    public interface ISyncRepository<T> where T : BaseEntity
    {
        T GetById(int id);
        T GetSingleBySpec(ISpecification<T> spec);
        IReadOnlyList<T> ListAll();
        IReadOnlyList<T> List(ISpecification<T> spec);
        T Add(T entity);
        T Update(T entity);
        void Delete(T entity);
        int Count(ISpecification<T> spec);
    }
}
