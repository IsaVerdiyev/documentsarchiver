﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IFolderService
    {
        IReadOnlyList<Folder> GetFolders();
        Task<IReadOnlyList<Folder>> GetFoldersAsync();

        Folder AddFolder(Folder folder);
        Task<Folder> AddFolderAsync(Folder folder);

        Folder ModifyFolder(Folder folder);
        Task<Folder> ModifyFolderAsync(Folder folder);

        Folder GetFolderByIdWithScanFiles(int folderId);
        Task<Folder> GetFolderByIdWithScanFilesAsync(int folderId);

        Folder GetFolderByIdWithData(int folderId);
        Task<Folder> GetFolderByIdWithDataAsync(int folderId);

        void DeleteFolder(int folderId);
        Task DeleteFolderAsync(int folderId);

        bool CheckIfFolderExistsById(int folderId);
        Task<bool> CheckIfFolderExistsByIdAsync(int folderId);
    }
}
