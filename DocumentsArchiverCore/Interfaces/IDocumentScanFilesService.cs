﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IDocumentScanFilesService
    {
        IEnumerable<DocumentScanFile> GetDocumentScanFilesOfDocument(int documentId);
        Task<IEnumerable<DocumentScanFile>> GetDocumentScanFilesOfDocumentAsync(int documentId);

        void DeleteDocumentScanfileById(int documentScanfileId);
        Task DeleteDocumentScanfileByIdAsync(int documentScanfileId);


    }
}
