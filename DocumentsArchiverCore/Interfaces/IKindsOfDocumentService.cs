﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IKindsOfDocumentService
    {
        IEnumerable<KindOfDocument> GetAllKindsOfDocument();
        Task<IEnumerable<KindOfDocument>> GetAllKindsOfDocumentAsync();

        KindOfDocument AddKindOfDocument(KindOfDocument newKindOfDocument);
        Task<KindOfDocument> AddKindOfDocumentAsync(KindOfDocument newKindOfDocument);

        KindOfDocument GetKindOfDocumentById(int kindOfDocumentId);
        Task<KindOfDocument> GetKindOfDocumentByIdAsync(int kindOfDocumentId);
    }
}
