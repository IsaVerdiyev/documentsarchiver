﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IRepository<T> : ISyncRepository<T>, IAsyncRepository<T> where T : BaseEntity
    {
    }
}
