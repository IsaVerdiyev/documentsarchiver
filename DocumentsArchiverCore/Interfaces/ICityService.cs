﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface ICityService
    {
        IReadOnlyList<City> GetCities();
        Task<IReadOnlyList<City>> GetCitiesAsync();

        City GetCityById(int id);
        Task<City> GetCityByIdAsync(int id);

        City AddCity(City city);
        Task<City> AddCityAsync(City city);
    }
}
