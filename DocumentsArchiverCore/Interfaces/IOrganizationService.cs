﻿using DocumentsArchiverCore.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IOrganizationService
    {
        IReadOnlyList<Organization> GetOrganizations();
        Task<IReadOnlyList<Organization>> GetOrganizationsAsync();

        Organization GetOrganizationById(int id);
        Task<Organization> GetOrganizationByIdAsync(int id);

        Organization AddOrganization(Organization organization);
        Task<Organization> AddOrganizationAsync(Organization organization);

    }
}
