﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DocumentsArchiverCore.Interfaces
{
    public interface IFileDeleter
    {
        void DeleteFile(string filePath);
        Task DeleteFileAsync(string filePath);
    }
}
