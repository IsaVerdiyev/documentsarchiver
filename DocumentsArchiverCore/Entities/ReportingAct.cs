﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class ReportingAct: BaseEntity
    {
        public ReportingScanFile ReportingScanFile { get; set; }
        public Document Document { get; set; }
    }
}
