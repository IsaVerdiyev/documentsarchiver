﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class FolderScanFile: BaseEntity
    {
        public string ScanFilePath { get; set; }

        public int FolderId { get; set; }
        public Folder Folder { get; set; }
    }
}
