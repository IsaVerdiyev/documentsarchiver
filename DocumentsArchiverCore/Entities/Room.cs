﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class Room: BaseEntity
    {
        public string RoomNumber { get; set; }
    }
}
