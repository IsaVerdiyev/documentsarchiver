﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class Location: BaseEntity
    {
        public int CityId { get; set; }
        public City City { get; set; }

        public string Address { get; set; }
    }
}
