﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public abstract class BaseEntity
    {
        public int Id { get; set; }
    }
}
