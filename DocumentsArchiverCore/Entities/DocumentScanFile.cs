﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class DocumentScanFile: BaseEntity
    {
        public int DocumentId { get; set; }
        public Document Document { get; set; }

        public string ScanFilePath { get; set; }
    }
}
