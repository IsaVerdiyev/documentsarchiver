﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class City:BaseEntity
    {
        public string Name { get; set; }
    }
}
