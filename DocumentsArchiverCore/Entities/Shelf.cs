﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class Shelf: BaseEntity
    {
        public int Number { get; set; }
    }
}
