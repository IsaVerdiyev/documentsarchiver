﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class Document: BaseEntity
    {
        public string NumberOfDocument { get; set; }

        public int KindOfDocumentId { get; set; }
        public KindOfDocument KindOfDocument { get; set; }

        public DateTime DateOfDocument { get; set; }

        public int TypeOfDocumentId { get; set; }
        public TypeOfDocument TypeOfDocument { get; set; }

        public int StructuralDepartmentId { get; set; }
        public StructuralDepartment StructuralDepartment { get; set; }

        public string IntroducerInitials { get; set; }

        public ReportingAct ReportingAct { get; set; }

        public string ExecuterInitials { get; set; }

        public DateTime ExecutionDate { get; set; }

        public string Note { get; set; }

        public int FolderId { get; set; }
        public Folder Folder { get; set; }

        public ICollection<DocumentScanFile> ScanFiles { get; set; }

    }
}
