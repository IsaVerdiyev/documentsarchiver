﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class FolderType: BaseEntity
    {
        public string Name { get; set; }
    }
}
