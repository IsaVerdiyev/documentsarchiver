﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class Organization: BaseEntity
    {
        public string Name { get; set; }
    }
}
