﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class Folder: BaseEntity
    {
        public int OrganizationId { get; set; }
        public Organization Organization { get; set; }

        public int LocationId { get; set; }
        public Location Location { get; set; }

        public int FloorId { get; set; }
        public Floor Floor { get; set; }

        public int RoomId { get; set; }
        public Room Room { get; set; }

        public int ShelvingColumnId { get; set; }
        public ShelvingColumn ShelvingColumn { get; set; }

        public int ShelvingId { get; set; }
        public Shelving Shelving { get; set; }

        public int ShelfId { get; set; }
        public Shelf Shelf { get; set; }

        public int FolderTypeId { get; set; }
        public FolderType FolderType { get; set; }

        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }

        public string Note { get; set; }

        public ICollection<FolderScanFile> FolderScanFiles { get; set; }

        public ICollection<Document> Documents { get; set; }
    }
}
