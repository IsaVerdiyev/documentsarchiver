﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class ReportingScanFile: BaseEntity
    {
        public ReportingAct ReportingAct { get; set; }
        public string ScanFilePath { get; set; }
    }
}
