﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DocumentsArchiverCore.Entities
{
    public class StructuralDepartment: BaseEntity
    {
        public string Name { get; set; }
        public int NomenclatureNumber { get; set; }
    }
}
