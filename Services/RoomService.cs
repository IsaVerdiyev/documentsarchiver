﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class RoomService : IRoomService
    {
        private readonly IRepository<Room> roomRepository;

        public RoomService(IRepository<Room> roomRepository)
        {
            this.roomRepository = roomRepository;
        }

        public Room AddRoom(Room room)
        {
            return roomRepository.Add(room);
        }

        public async Task<Room> AddRoomAsync(Room room)
        {
            return await roomRepository.AddAsync(room);
        }

        public Room GetRoomById(int id)
        {
            return roomRepository.GetById(id);
        }

        public async Task<Room> GetRoomByIdAsync(int id)
        {
            return await roomRepository.GetByIdAsync(id);
        }

        public IReadOnlyList<Room> GetRooms()
        {
            return roomRepository.ListAll();
        }

        public async Task<IReadOnlyList<Room>> GetRoomsAsync()
        {
            return await roomRepository.ListAllAsync();
        }
    }
}
