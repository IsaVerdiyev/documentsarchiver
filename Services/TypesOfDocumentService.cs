﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class TypesOfDocumentService : ITypesOfDocumentService
    {
        private readonly IRepository<TypeOfDocument> typesOfDocumentRepository;

        public TypesOfDocumentService(IRepository<TypeOfDocument> typesOfDocumentRepository)
        {
            this.typesOfDocumentRepository = typesOfDocumentRepository;
        }

        public IEnumerable<TypeOfDocument> GetAllTypesOfDocument()
        {
            return typesOfDocumentRepository.ListAll();
        }

        public async Task<IEnumerable<TypeOfDocument>> GetAllTypesOfDocumentAsync()
        {
            return await typesOfDocumentRepository.ListAllAsync();
        }

        public TypeOfDocument AddTypeOfDocument(TypeOfDocument typeOfDocument)
        {
            return typesOfDocumentRepository.Add(typeOfDocument);
        }

        public async Task<TypeOfDocument> AddTypeOfDocumentAsync(TypeOfDocument typeOfDocument)
        {
            return await typesOfDocumentRepository.AddAsync(typeOfDocument);
        }

        public TypeOfDocument GetTypeOfDocumentById(int typeOfDocumentId)
        {
            return typesOfDocumentRepository.GetById(typeOfDocumentId);
        }

        public async Task<TypeOfDocument> GetTypeOfDocumentIdAsync(int typeOfDocumentId)
        {
            return await typesOfDocumentRepository.GetByIdAsync(typeOfDocumentId);
        }
    }
}
