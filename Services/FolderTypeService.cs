﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class FolderTypeService : IFolderTypeService
    {
        private readonly IRepository<FolderType> folderTypeRepository;

        public FolderTypeService(IRepository<FolderType> folderTypeRepository)
        {
            this.folderTypeRepository = folderTypeRepository;
        }

        public void AddFolderType(FolderType containerType)
        {
            folderTypeRepository.Add(containerType);
        }

        public async Task AddFolderTypeAsync(FolderType containerType)
        {
            await folderTypeRepository.AddAsync(containerType);
        }

        public FolderType GetFolderTypeById(int id)
        {
            return folderTypeRepository.GetById(id);
        }

        public async Task<FolderType> GetFolderTypeByIdAsync(int id)
        {
            return await folderTypeRepository.GetByIdAsync(id);
        }

        public IReadOnlyList<FolderType> GetFolderTypes()
        {
            return folderTypeRepository.ListAll();
        }

        public async Task<IReadOnlyList<FolderType>> GetFolderTypesAsync()
        {
            return await folderTypeRepository.ListAllAsync();
        }
    }
}
