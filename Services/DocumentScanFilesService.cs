﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using DocumentsArchiverCore.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DocumentScanFilesService : IDocumentScanFilesService
    {
        private readonly IRepository<DocumentScanFile> documentScanFilesRepository;
        private readonly IFileDeleter fileDeleter;

        public DocumentScanFilesService(IRepository<DocumentScanFile> documentScanFilesRepository, IFileDeleter fileDeleter)
        {
            this.documentScanFilesRepository = documentScanFilesRepository;
            this.fileDeleter = fileDeleter;
        }

        public void DeleteDocumentScanfileById(int documentScanfileId)
        {
            DocumentScanFile deletedDocumentScanfile = documentScanFilesRepository.GetById(documentScanfileId);

            try
            {
                string filePath = deletedDocumentScanfile.ScanFilePath;
                documentScanFilesRepository.Delete(deletedDocumentScanfile);
                fileDeleter.DeleteFile(filePath);
            }
            catch (Exception ex)
            {
                if (documentScanFilesRepository.GetById(documentScanfileId) == null)
                {
                    documentScanFilesRepository.Add(deletedDocumentScanfile);
                }
                throw;
            }
        }

        public async Task DeleteDocumentScanfileByIdAsync(int documentScanfileId)
        {

            DocumentScanFile deletedDocumentScanfile = await documentScanFilesRepository.GetByIdAsync(documentScanfileId);
            try
            {
                string filePath = deletedDocumentScanfile.ScanFilePath;
                await documentScanFilesRepository.DeleteAsync(deletedDocumentScanfile);
                await fileDeleter.DeleteFileAsync(filePath);
            }
            catch (Exception ex)
            {
                if (await documentScanFilesRepository.GetByIdAsync(documentScanfileId) == null)
                {
                    await documentScanFilesRepository.AddAsync(deletedDocumentScanfile);
                }
                throw;
            }
        }

        public IEnumerable<DocumentScanFile> GetDocumentScanFilesOfDocument(int documentId)
        {
            return documentScanFilesRepository.List(new DocumentScanFilesByDocumentSpecification(documentId));
        }

        public async Task<IEnumerable<DocumentScanFile>> GetDocumentScanFilesOfDocumentAsync(int documentId)
        {
            return await documentScanFilesRepository.ListAsync(new DocumentScanFilesByDocumentSpecification(documentId));
        }
    }
}
