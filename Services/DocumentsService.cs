﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using DocumentsArchiverCore.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class DocumentsService : IDocumentsService
    {
        private readonly IRepository<Document> documentsRepository;

        public DocumentsService(IRepository<Document> documentsRepository)
        {
            this.documentsRepository = documentsRepository;
        }

        public Document AddDocument(Document document)
        {
            return documentsRepository.Add(document);
        }

        public async Task<Document> AddDocumentAsync(Document document)
        {
            return await documentsRepository.AddAsync(document);
        }

        public void DeleteDocument(int documentId)
        {
            documentsRepository.Delete(documentsRepository.GetById(documentId));
        }

        public async Task DeleteDocumentAsync(int documentId)
        {
            await documentsRepository.DeleteAsync(await documentsRepository.GetByIdAsync(documentId));
        }

        public Document GetDocumentById(int documentId)
        {
            return documentsRepository.GetById(documentId);
        }

        public async Task<Document> GetDocumentByIdAsync(int documentId)
        {
            return await documentsRepository.GetByIdAsync(documentId);
        }

        public Document GetDocumentByIdWithScanFiles(int documentId)
        {
            return documentsRepository.GetSingleBySpec(new DocumentWithScanfilesSpecification(documentId));
        }

        public async Task<Document> GetDocumentByIdWithScanFilesAsync(int documentId)
        {
            return await documentsRepository.GetSingleBySpecAsync(new DocumentWithScanfilesSpecification(documentId));
        }

        public IEnumerable<Document> GetDocumentsOfFolder(int folderId)
        {
            return documentsRepository.List(new DocumentsByFolderSpecification(folderId));
        }

        public async Task<IEnumerable<Document>> GetDocumentsOfFolderAsync(int folderId)
        {
            return await documentsRepository.ListAsync(new DocumentsByFolderSpecification(folderId));
        }

        public Document ModifyDocument(Document document)
        {
            return documentsRepository.Update(document);
        }

        public async Task<Document> ModifyDocumentAsync(Document document)
        {
            return await documentsRepository.UpdateAsync(document);
        }
    }
}
