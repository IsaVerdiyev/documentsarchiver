﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class CityService : ICityService
    {
        private readonly IRepository<City> cityRepository;

        public CityService(IRepository<City> cityRepository)
        {
            this.cityRepository = cityRepository;
        }

        public City AddCity(City city)
        {
            return cityRepository.Add(city);
        }

        public async Task<City> AddCityAsync(City city)
        {
            return await cityRepository.AddAsync(city);
        }

        public IReadOnlyList<City> GetCities()
        {
            return cityRepository.ListAll();
        }

        public async Task<IReadOnlyList<City>> GetCitiesAsync()
        {
            return await cityRepository.ListAllAsync();
        }

        public City GetCityById(int id)
        {
            return cityRepository.GetById(id);
        }

        public async Task<City> GetCityByIdAsync(int id)
        {
            return await cityRepository.GetByIdAsync(id);
        }
    }
}
