﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using DocumentsArchiverCore.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class FolderScanFileService : IFolderScanFileService
    {
        private readonly IRepository<FolderScanFile> folderScanFilesRepository;
        private readonly IFileDeleter fileDeleter;

        public FolderScanFileService(IRepository<FolderScanFile> folderScanFilesRepository, IFileDeleter fileDeleter)
        {
            this.folderScanFilesRepository = folderScanFilesRepository;
            this.fileDeleter = fileDeleter;
        }

        public void DeleteFolderScanfileById(int folderScanfileId)
        {
            FolderScanFile deletedFolderScanfile = folderScanFilesRepository.GetById(folderScanfileId);
            try
            {
                string filePath = deletedFolderScanfile.ScanFilePath;
                folderScanFilesRepository.Delete(deletedFolderScanfile);
                fileDeleter.DeleteFile(filePath);
            }
            catch(Exception ex)
            {
                if(folderScanFilesRepository.GetById(folderScanfileId) == null)
                {
                    folderScanFilesRepository.Add(deletedFolderScanfile);
                }
                throw;
            }
        }

        public async Task DeleteFolderScanfileByIdAsync(int folderScanfileId)
        {
            FolderScanFile deletedFolderScanfile = await folderScanFilesRepository.GetByIdAsync(folderScanfileId);
            try
            {
                string filePath = deletedFolderScanfile.ScanFilePath;
                await folderScanFilesRepository.DeleteAsync(deletedFolderScanfile);
                await fileDeleter.DeleteFileAsync(filePath);
            }
            catch (Exception ex)
            {
                if (await folderScanFilesRepository.GetByIdAsync(folderScanfileId) == null)
                {
                    await folderScanFilesRepository.AddAsync(deletedFolderScanfile);
                }
                throw;
            }
        }

        public async Task<FolderScanFile> GetFolderScanFileAsync(int folderScanFileId)
        {
            return await folderScanFilesRepository.GetByIdAsync(folderScanFileId);
        }

        public FolderScanFile GetFolderScanFileById(int folderScanFileId)
        {
            return folderScanFilesRepository.GetById(folderScanFileId);
        }

        public async Task<IEnumerable<FolderScanFile>> GetFolderScanFilesAsync(int folderId)
        {
            return await folderScanFilesRepository.ListAsync(new FolderScanFilesByFolderSpecification(folderId));
        }

        public IEnumerable<FolderScanFile> GetScanFilesOfFolder(int folderId)
        {
            return folderScanFilesRepository.List(new FolderScanFilesByFolderSpecification(folderId));
        }
    }
}
