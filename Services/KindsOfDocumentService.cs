﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class KindsOfDocumentService : IKindsOfDocumentService
    {
        private readonly IRepository<KindOfDocument> kindsOfDocumentRepository;

        public KindsOfDocumentService(IRepository<KindOfDocument> kindsOfDocumentRepository)
        {
            this.kindsOfDocumentRepository = kindsOfDocumentRepository;
        }

        public KindOfDocument AddKindOfDocument(KindOfDocument newKindOfDocument)
        {
            return kindsOfDocumentRepository.Add(newKindOfDocument);
        }

        public async Task<KindOfDocument> AddKindOfDocumentAsync(KindOfDocument newKindOfDocument)
        {
            return await kindsOfDocumentRepository.AddAsync(newKindOfDocument);
        }

        public IEnumerable<KindOfDocument> GetAllKindsOfDocument()
        {
            return kindsOfDocumentRepository.ListAll();
        }

        public async Task<IEnumerable<KindOfDocument>> GetAllKindsOfDocumentAsync()
        {
            return await kindsOfDocumentRepository.ListAllAsync();
        }

        public KindOfDocument GetKindOfDocumentById(int kindOfDocumentId)
        {
            return kindsOfDocumentRepository.GetById(kindOfDocumentId);
        }

        public async Task<KindOfDocument> GetKindOfDocumentByIdAsync(int kindOfDocumentId)
        {
            return await kindsOfDocumentRepository.GetByIdAsync(kindOfDocumentId);
        }
    }
}
