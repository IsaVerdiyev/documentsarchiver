﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using DocumentsArchiverCore.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class FolderService : IFolderService
    {
        private readonly IRepository<Folder> folderRepository;

        public FolderService(IRepository<Folder> folderRepository)
        {
            this.folderRepository = folderRepository;
        }

        public Folder AddFolder(Folder folder)
        {
            return folderRepository.Add(folder);
        }

        public async Task<Folder> AddFolderAsync(Folder folder)
        {
            return await folderRepository.AddAsync(folder);
        }

        public bool CheckIfFolderExistsById(int folderId)
        {
            return folderRepository.GetById(folderId) != null;
        }

        public async Task<bool> CheckIfFolderExistsByIdAsync(int folderId)
        {
            return await folderRepository.GetByIdAsync(folderId) != null;
        }

        public void DeleteFolder(int folderId)
        {
            folderRepository.Delete(folderRepository.GetById(folderId));
        }

        public async Task DeleteFolderAsync(int folderId)
        {
            await folderRepository.DeleteAsync(await folderRepository.GetByIdAsync(folderId));
        }

        public Folder GetFolderByIdWithData(int folderId)
        {
            return folderRepository.GetSingleBySpec(new FolderWithAllWithoutScanFilesSpecification(folderId));
        }

        public async Task<Folder> GetFolderByIdWithDataAsync(int folderId)
        {
            return await folderRepository.GetSingleBySpecAsync(new FolderWithAllWithoutScanFilesSpecification(folderId));
        }

        public Folder GetFolderByIdWithScanFiles(int folderId)
        {
            return folderRepository.GetSingleBySpec(new FolderWitScanFilesSpecification(folderId));
        }

        public async Task<Folder> GetFolderByIdWithScanFilesAsync(int folderId)
        {
            return await folderRepository.GetSingleBySpecAsync(new FolderWitScanFilesSpecification(folderId));
        }

        public IReadOnlyList<Folder> GetFolders()
        {
            return folderRepository.List(new FoldersWithAllIncludeSpecification());
        }

        public async Task<IReadOnlyList<Folder>> GetFoldersAsync()
        {
            return await folderRepository.ListAsync(new FoldersWithAllIncludeSpecification());
        }

        public Folder ModifyFolder(Folder folder)
        {
            return folderRepository.Update(folder);
        }

        public async Task<Folder> ModifyFolderAsync(Folder folder)
        {
            return await folderRepository.UpdateAsync(folder);
        }
    }
}
