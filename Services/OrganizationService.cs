﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class OrganizationService : IOrganizationService
    {
        private readonly IRepository<Organization> organizationRepository;

        public OrganizationService(IRepository<Organization> organizationRepository)
        {
            this.organizationRepository = organizationRepository;
        }

        public Organization AddOrganization(Organization organization)
        {
            return organizationRepository.Add(organization);
        }

        public async Task<Organization> AddOrganizationAsync(Organization organization)
        {
            return await organizationRepository.AddAsync(organization);
        }

        public Organization GetOrganizationById(int id)
        {
            return organizationRepository.GetById(id);
        }

        public async Task<Organization> GetOrganizationByIdAsync(int id)
        {
            return await organizationRepository.GetByIdAsync(id);
        }

        public IReadOnlyList<Organization> GetOrganizations()
        {
            return organizationRepository.ListAll();
        }

        public async Task<IReadOnlyList<Organization>> GetOrganizationsAsync()
        {
            return await organizationRepository.ListAllAsync();
        }
    }
}
