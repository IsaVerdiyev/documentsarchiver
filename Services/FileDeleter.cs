﻿using DocumentsArchiverCore.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class FileDeleter : IFileDeleter
    {
        public void DeleteFile(string filePath)
        {
            File.Delete(filePath);
        }

        public async Task DeleteFileAsync(string filePath)
        {
            await Task.Run(() => {
                File.Delete(filePath);
            });
        }
    }
}
