﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using DocumentsArchiverCore.Specifications;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class LocationService : ILocationService
    {
        private readonly IRepository<Location> locationRepository;

        public LocationService(IRepository<Location> locationRepository)
        {
            this.locationRepository = locationRepository;
        }

        public Location AddLocation(Location location)
        {
            return locationRepository.Add(location);
        }

        public async Task<Location> AddLocationAsync(Location location)
        {
            return await locationRepository.AddAsync(location);
        }

        public Location GetLocationById(int id)
        {
            return locationRepository.GetById(id);
        }

        public async Task<Location> GetLocationByIdAsync(int id)
        {
            return await locationRepository.GetByIdAsync(id);
        }

        public IReadOnlyList<Location> GetLocations()
        {
            return locationRepository.List(new AllLocationsWithCitiesIncludeSpecification());
        }

        public async Task<IReadOnlyList<Location>> GetLocationsAsync()
        {
            return await locationRepository.ListAsync(new AllLocationsWithCitiesIncludeSpecification());
        }
    }
}
