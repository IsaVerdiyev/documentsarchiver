﻿using DocumentsArchiverCore.Entities;
using DocumentsArchiverCore.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class StructuralDepartmentsService : IStructuralDepartmentsService
    {
        private readonly IRepository<StructuralDepartment> structuralDepartmentsRepository;

        public StructuralDepartmentsService(IRepository<StructuralDepartment> structuralDepartmentsRepository)
        {
            this.structuralDepartmentsRepository = structuralDepartmentsRepository;
        }

        public StructuralDepartment AddStructuralDepartment(StructuralDepartment structuralDepartment)
        {
            return structuralDepartmentsRepository.Add(structuralDepartment);
        }

        public async Task<StructuralDepartment> AddStructuralDepartmentAsync(StructuralDepartment structuralDepartment)
        {
            return await structuralDepartmentsRepository.AddAsync(structuralDepartment);
        }

        public IEnumerable<StructuralDepartment> GetAllStructuralDepartments()
        {
            return structuralDepartmentsRepository.ListAll();
        }

        public async Task<IEnumerable<StructuralDepartment>> GetAllStructuralDepartmentsAsync()
        {
            return await structuralDepartmentsRepository.ListAllAsync();
        }

        public StructuralDepartment GetStructuralDepartmentById(int structuralDepartmentId)
        {
            return structuralDepartmentsRepository.GetById(structuralDepartmentId);
        }

        public async Task<StructuralDepartment> GetStructuralDepartmentByIdAsync(int structuralDepartmentId)
        {
            return await structuralDepartmentsRepository.GetByIdAsync(structuralDepartmentId);
        }
    }
}
